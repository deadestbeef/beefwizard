/**************************************************************************************************
 * File:        game_server.h
 *
 * Purpose:     The job of Game Server is to handle all game-level services. It knows which
 *              players are connected, the state of all players and entities, and handles the
 *              calculations needed to update the state. It also handles the act of writing data
 *              back to those players.
 *
 * **************************************************************************************************/
#ifndef GAME_SERVER_H
#define GAME_SERVER_H

#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>
#include <string>
#include "connections.h"
#include "player_data.h"
#include "error_codes.h"
#include "game_constants.h"

enum class GameState
{
    Lobby, Loading, Game, Cleanup, PostGame
};

struct ProjectileData
{
    uint8_t id;
    ProjectileTypes type;
    sf::Vector2f velocity; // Speed and direction; Pixels-per-second
    float time_to_live; // seconds
    float safety_timeout; // seconds
    sf::Vector2f position;
    float orientation; // degrees, 0-359
    float radius;
};

class GameServer
{
public:
    GameServer(int id, PlayerData host);
    ~GameServer() = default;

    GameState Update();

    void JoinGame(PlayerData& player);
    bool ExitGame(PlayerData& player, bool player_exists = true);
    void BeginGame(PlayerData& player);
    void Chat(PlayerData& player);

    void UpdateState(PlayerData& player);
    void CreateProjectile(ProjectileData& fireball, int player_id);
    void NotifyCollision(PlayerData&, ProjectileData&);
    void NotifyLoaded(int32_t player_id);

    void Finalize();
    void FinalizeACK(int player_id);

    std::string GetName() const;
    int GetHostID() const;
    int GetServerID() const;
    std::vector<PlayerData> GetPlayers() const;
    GameState GetState() const;
    int GetNumPlayers() const;

private:
    bool updateGame();
    bool sendError(PlayerData& player, Error e);
    void startMatch();
    bool sendStateUpdate();
    void loadingGame();
    bool updateRank(PlayerData& player);
    void updatePlayers(float elapsed);
    void addFriction(PlayerData& player, float elapsed);

    int id;
    std::string game_name;
    GameState state;
    sf::Clock clock;
    sf::Clock start_clock;
    int hosting_player;
    float island_radius;
    std::vector<PlayerData> players;
    std::vector<ProjectileData> projectiles;
    int32_t num_collisions;
    std::vector<int> player_status;

    sf::Clock sendClock;
    bool OKToSend;
};

#endif // GAME_SERVER_H
