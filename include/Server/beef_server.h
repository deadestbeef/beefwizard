/**************************************************************************************************
 * File:        beef_server.h
 *
 * Purpose:     BeefServer is a top-level server that handles the serialization and
 *              deserialization of network messages, and routes them to and from the appropriate
 *              game servers.  It also handles logging players in and out.
 *
**************************************************************************************************/
#ifndef BEEF_SERVER_H
#define BEEF_SERVER_H

#include <vector>
#include <string>
#include <memory>
#include "game_server.h"
#include "connections.h"
#include "error_codes.h"
#include "scoreboard.h"

class BeefServer
{
public:
    BeefServer() = default;
    ~BeefServer();

    void Update();

private:
    // A list of all players currently logged in
    std::vector<PlayerData> players;

    // A list of all currently active game servers. The index of the server is the GameID.
    std::vector<GameServer> game_servers;

    //the main listener for incoming tcp connections
    TCPListener tcp_listener;

    // This may be replaced with Tcp Socket objects or something similar, but the idea is
    // to be able to enumerate through all open connections and read anything inside them.
    // Probably we'll also need a way to identify each open connection with a player name.
    std::vector<std::shared_ptr<TCPSocket>> open_tcp_connections;

    Scoreboard scoreboard;

    bool registerUser(std::shared_ptr<TCPSocket>&);
    void createGame(PlayerData& player);
    void listGame(PlayerData& player);
    void joinGame(PlayerData& player);
    void exitGame(PlayerData& player);
    void beginGame(PlayerData& player);
    void unregister(PlayerData& player);
    void chat(PlayerData& player);
    void scoreboardRequest(PlayerData& player);
    void notifyLoaded(PlayerData& player);
    void stateUpdate(PlayerData& player);
    void newProjectile(PlayerData& player);
    void gameFinalization(PlayerData& player);

    bool sendError(PlayerData& player, Error e);
    bool sendError(std::shared_ptr<TCPSocket>& socket, Error e);
};

#endif // BEEF_SERVER_H
