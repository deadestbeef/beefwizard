#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include <string>
#include <vector>
#include "player_data.h"

class PlayerScore
{
public:
    PlayerScore() = default;
    PlayerScore(std::string name);

    void AddRank(int rank);
    void AddSurvivalTime(float time);

    std::string GetName();
    int GetNumGames();
    float GetAverageRank() const;
    float GetAverageTime() const;
    int GetNumWins();

    const std::vector<int>& GetRankings();
    const std::vector<float>& GetSurvivalTimes();

private:
    std::string name;
    std::vector<int> rankings;
    std::vector<float> survival_times;
    int32_t num_wins;
};

class Scoreboard
{
public:
    Scoreboard();
    ~Scoreboard();

    void AddRecords(std::vector<PlayerData>& players);

    std::vector<PlayerScore>& GetRecordsByRank();
    std::vector<PlayerScore>& GetRecordsByTime();

private:
    std::vector<PlayerScore> player_records;
};

#endif // SCOREBOARD_H