#ifndef PLAYER_DATA_H
#define PLAYER_DATA_H

#include <SFML/System/Vector2.hpp>
#include <SFML/System/Clock.hpp>
#include <memory>
#include "connections.h"
#include "game_constants.h"

class PlayerData
{
public:
    PlayerData() = delete;
    PlayerData(std::string name, std::shared_ptr<TCPSocket> socket, int player_id);

    float GetVelocityMagnitude();
    void UpdateVelocity();
    void StartClock();

    bool operator==(const PlayerData& other) const;
    bool operator<(const PlayerData& other) const;
    bool operator>(const PlayerData& other) const;
    bool operator<=(const PlayerData& other) const;
    bool operator>=(const PlayerData& other) const;

    std::string name;
    std::shared_ptr<TCPSocket> socket;
    int32_t player_id;
    int32_t game_id;
    PlayerStatus status;
    bool is_loaded;
    float health;
    sf::Vector2f position;
    sf::Vector2f velocity; // Direction and speed; pixels per second
    float orientation; // Degrees, 0-359
    bool in_lava;
    int8_t rank;
    float survival_time; // Seconds, decided at end of game
    bool was_updated;
    sf::Clock clock;
    float w;
    float a;
    float s;
    float d;
    float frozen;
};

#endif // PLAYER_DATA_H
