#ifndef COLORS_H
#define COLORS_H

#include <cstdint>

struct Color
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

const Color Red = { 200, 22, 22 };
const Color Orange = { 255, 147, 40 };
const Color Teal = { 33, 255, 195 };
const Color Green = { 33, 175, 54 };
const Color Blue = { 33, 95, 175 };
const Color Pink = { 235, 163, 255 };
const Color Purple = { 150, 10, 145 };

#endif // COLORS_H
