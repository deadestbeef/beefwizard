#ifndef ERROR_CODES_H
#define ERROR_CODES_H

#include <cstdint>

enum class Error : uint16_t
{
    NotLoggedIn = 0,
    NameNotAvailable,
    AlreadyHosting,
    AlreadyInGame,
    GameNotFound,
	AlreadyLoaded,
    GameAlreadyStarted,
    GameIsFull,
    HostLeft,
    NotInGame,
    GameStartedCannotLeave,
    GameStartedCannotStart,
    CannotStartNotHost,
    CannotChatNotInGame,
    CannotChatServerDoesNotExist,
    CannotCreateProjectileGameDoesNotExist
    // If you add a code, add an associated string :)
};

std::string GetErrorString(Error code);

#endif // ERROR_CODES_H
