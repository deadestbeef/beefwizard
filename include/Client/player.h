#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include <string>
#include "game_constants.h"

class Player
{
public:
    Player();

    void setPlayerName(std::string name);
    void setPlayerColor(sf::Color color);
    void setPosition(sf::Vector2f position);
    sf::Vector2f getPosition();
    void clearHealth();
    void setAsPlayer();

    PlayerStatus getStatus();

    void update(sf::Vector2f position, float orientation, sf::Vector2f velocity,
                float health, PlayerStatus status);
    void deadReckoningUpdate();
    void draw(sf::RenderWindow& window);

    void SetFrozen();

private:
    sf::Sprite baseSprite;
    sf::Sprite shadingSprite;
    sf::Sprite hatSprite;
    sf::Sprite iceblockSprite;
    sf::RectangleShape healthBar;
    sf::Text playerName;
    sf::Vector2f velocity;
    PlayerStatus status;
    bool frozen;
    sf::Clock clock;
    float elapsed;
};

#endif // PLAYER_H