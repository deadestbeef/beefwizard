#ifndef LOGIN_H
#define LOGIN_H

#include "game_scene.h"
#include "popup.h"
#include <string>

/**
 * Login is the screen that displays when first launching the game. It allows a
 * user to log in to the server
 */
class Login: public GameScene
{

public:
    Login(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:
    bool registerPlayer(std::string playerName);

    const int MAX_NAME_WIDTH = 300;
    uint32_t m_screenWidth;
    sf::RenderTexture renderTexture;
    sf::Font m_font;
    sf::Text m_promptText;
    sf::Text m_userNameText;
    sf::Texture m_logoTexture;
    sf::Sprite m_logoSprite;

    sf::String m_text;
    sf::Text m_inputText;
    sf::Texture m_boxTexture;
    sf::Sprite m_boxSprite;
    sf::Texture m_cursorTexture;
    sf::Sprite m_cursorSprite;
    sf::Time m_cursorCounter;
    bool m_cursorShowing;

    Popup popup;
    bool displayingPopup;
};

#endif //LOGIN_H
