#ifndef TUTORIAL_SCENE_H
#define TUTORIAL_SCENE_H

#include "game_constants.h"
#include "game_scene.h"
#include "sprite_animation.h"
#include <string>

/**
 * TutorialScene is the scene that displays directions on how to play the game.
 */
class TutorialScene: public GameScene
{

public:
    TutorialScene(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:
    sf::Vector2i aspect_ratio;

    sf::Text howToPlayText;
    sf::Texture backButtonTexture;
    sf::Sprite backButtonSprite;

    sf::Text movementText;
    sf::Text fireballText;
    sf::Text iceballText;

    sf::Texture wasdTexture;
    sf::Sprite wasdSprite;
    sf::Texture mouseTexture;
    sf::Sprite leftClickSprite;
    sf::Sprite rightClickSprite;

    sf::Texture movementSpriteSheet;
    sf::Texture fireballSpriteSheet;
    sf::Texture iceballSpriteSheet;

    SpriteAnimation movementAnimation;
    SpriteAnimation fireballAnimation;
    SpriteAnimation iceballAnimation;


};

#endif //TUTORIAL_SCENE_H
