#ifndef FIREBALL_H
#define FIREBALL_H

#include <SFML/Graphics.hpp>
#include <cstdint>

class Fireball
{
public:
    Fireball() = delete;
    Fireball(sf::Vector2f position, sf::Vector2f velocity, float rotation, uint8_t ID);

    bool Update(sf::Time elapsed);
    void Draw(sf::RenderWindow& window);

    uint8_t getID();

private:
    sf::Sprite sprite;
    sf::Vector2f velocity;
    uint8_t id;
    float time_to_live;
};

#endif // FIREBALL_H
