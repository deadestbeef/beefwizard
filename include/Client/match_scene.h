#ifndef MATCH_SCENE_H
#define MATCH_SCENE_H

#include "game_scene.h"
#include "connections.h"
#include "player.h"
#include "fireball.h"
#include "iceball.h"

#include <vector>
#include <string>
#include <list>
#include <deque>

/**
 * The MatcheScene is the scene that displays a match. It is the core of
 * BeefWizard.
 */
class MatchScene: public GameScene
{

public:
    MatchScene(sf::Vector2i ratio, TCPSocket& sock);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:
    void startGame();
    void updateState(sf::Time elapsed);

    sf::Vector2i aspect_ratio;
    sf::Vector2i original_ratio;
    bool arenaLoaded;
    TCPSocket& socket;
    int32_t playerCount;
    std::vector<Player> players;

    sf::Texture islandTexture;
    sf::Texture lavaTexture;

    sf::Sprite lavaSprite;
    sf::CircleShape islandShape;

    static const int MAX_CHAT_LINES = 5;
    static const int MAX_CHAT_WIDTH = 400;
    sf::Text ingameChat[MAX_CHAT_LINES];
    sf::Text playerChatText;
    std::deque<std::string> ingameChatStrings;
    std::string enteredText;
    bool activeChat;
    sf::Time lastMessageTime;
    sf::Font m_font;
    sf::Texture chatOverlayTexture;
    sf::Sprite chatOverlaySprite;

    sf::Texture cursorTexture;
    sf::Sprite cursorSprite;
    sf::Time cursorCounter;
    bool cursorShowing;

    sf::Vector2f velocity;
    sf::Vector2f controlled_acceleration;
    float lastElapsedTime;
    float fireball_cooldown;
    float iceball_cooldown;

    sf::Clock sendClock;
    bool okayToSend;

    float frozen;

    int playerIndex;

    sf::View arenaView;
    uint32_t sequenceNumber;
    std::vector<Fireball> fireballs;
    std::vector<Iceball> iceballs;

    void updateView(sf::View& view);
    void calculateMovement();
    void sendUpdateToServer(bool w, bool a, bool s, bool d);
    void createFireball(sf::RenderWindow& window);
    void createIceball(sf::RenderWindow& window);
    void receiveProjectileNotification();
    void handleCollision();
    void processChat(std::vector<sf::Event>& events);
    void sendChat();
};

#endif //MATCH_SCENE_H
