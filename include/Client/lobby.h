#ifndef LOBBY_H
#define LOBBY_H

#include "game_scene.h"
#include "connections.h"
#include "popup.h"
#include "game_constants.h"
#include <deque>

/**
 * MainMenu is the menu the player sees after they log in.
 */
class Lobby: public GameScene
{

public:
    Lobby(sf::Vector2i ratio, TCPSocket& sock);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:
    void exitLobby();
    void beginInitializeGame();
    void initializeGame();
    void sendChat();

    sf::Vector2i aspect_ratio;
    sf::Font m_font;
    sf::Text gameNameText;
    sf::Text playerNameTexts[MAX_PLAYERS];
    sf::Text playerChatText;
    static const int MAX_CHAT_LINES = 15;
    static const int MAX_CHAT_WIDTH = 600;
    std::deque<std::string> chatBox;
    sf::Text chatRows[MAX_CHAT_LINES];

    sf::Texture playerListTexture;
    sf::Texture playerChatBoxTexture;
    sf::Texture startButtonTexture;
    sf::Sprite playerListSprite;
    sf::Sprite playerChatBoxSprite;
    sf::Sprite startButtonSprite;
    TCPSocket& socket;

    sf::String enteredText;
    sf::Texture cursorTexture;
    sf::Sprite cursorSprite;

    sf::Texture backButtonTexture;
    sf::Sprite backButtonSprite;

    sf::Time cursorCounter;
    bool cursorShowing;



    bool isHosting;
    bool gameNameSet;
    int players;

    Popup popup;
    bool displayingPopup;
    bool leaveLobby;
};

#endif //LOBBY_H