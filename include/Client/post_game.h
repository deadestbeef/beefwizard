#ifndef POST_GAME_H
#define POST_GAME_H

#include "game_constants.h"
#include "game_scene.h"
#include "player.h"
#include <string>

/**
 * PostGame is the scene that displays the rankings after a game.
 */
class PostGame: public GameScene
{

public:
    PostGame(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:
    void loadRankings();

    sf::Vector2i aspect_ratio;

    int32_t playerCount;
    std::vector<Player> players;

    sf::Font font;
    sf::Text rankingsText[MAX_PLAYERS];
    sf::Text playerNames[MAX_PLAYERS];

    sf::Text backToMenuText;

    bool rankingsLoaded;
};

#endif //POST_GAME_H
