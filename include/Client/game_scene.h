#ifndef GAME_SCENE_H
#define GAME_SCENE_H

#include <SFML/Graphics.hpp>
#include "game_state.h"

/**
 * GameScene is the base class for all "scenes" in BeefWizard. A scene is
 * defined as a stand-alone set of objects to be displayed. Examples would be
 * the log in screen, the main menu, the scoreboard, and the game itself.
 */
class GameScene
{
public:

    GameScene(){}
    virtual ~GameScene(){};

    //reset the state after transitioning
    virtual void reset() = 0;

    //resize scene
    virtual void resize(sf::Vector2i ratio) = 0;

    //perform any tasks if exiting the game
    virtual void exit() = 0;

    //update the current state, returns next state to be in
    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) = 0;

    //do any drawing you need to
    virtual void draw(sf::RenderWindow& window) = 0;
};


#endif //GAME_SCENE_H