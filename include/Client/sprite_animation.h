#ifndef SPRITE_ANIMATION_H
#define SPRITE_ANIMATION_H

#include <SFML/Graphics.hpp>

class SpriteAnimation
{
public:
    SpriteAnimation() = default;
    SpriteAnimation(sf::Texture& texture, sf::Vector2i frameSize, sf::Vector2i frameGrid, float timeBetweenFrames);

    void setPosition(sf::Vector2f position);
    void update(float elapsedTime);
    void draw(sf::RenderWindow& window);

private:
    sf::Sprite m_sprite;
    sf::Vector2i m_frameSize;
    sf::Vector2i m_frameGrid;
    float m_timeBetweenFrames;
    float currentTime;
    sf::Vector2i currentFrame;

};


#endif //SPRITE_ANIMATION_H