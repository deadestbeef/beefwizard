#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "game_scene.h"

/**
 * MainMenu is the menu the player sees after they log in.
 */
class MainMenu: public GameScene
{

public:
    MainMenu(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:

    bool createGame();

    sf::Vector2i aspect_ratio;
    sf::Font m_font;
    sf::Text playerNameText;
    sf::Text findGameText;
    sf::Text createGameText;
    sf::Text leaderboardText;
    sf::Text howToPlayText;
    sf::Text exitText;

    bool playerNameSet;
};

#endif //MAIN_MENU_H