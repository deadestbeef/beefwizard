#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <string>
#include <cstdint>
#include <SFML/Graphics.hpp>

#include "game_scene.h"
#include "game_state.h"
#include "connections.h"
#include "game_constants.h"

/**
 * GameManager manages all states, transitions between states, and special
 * program data that is carried between states and their associated scenes.
 *
 * The special program data carried between states is handled with static member
 * variables that need to be requested. They should be guarenteed to be valid
 * when entering a state/scene that is known to use them.
 */
class GameManager
{
public:
    GameManager(sf::Vector2i ratio, const char* ip);
    ~GameManager();

    void Update(sf::RenderWindow& window);
    void Draw(sf::RenderWindow& window);

    static TCPSocket& getTCPSocket();

    static std::string getPlayerName();
    static void setPlayerName(std::string name);

    static int32_t getPlayerID();
    static void setPlayerID(uint32_t ID);

    static std::string getGameName();
    static void setGameName(std::string name);

    static int32_t getGameID();
    static void setGameID(int32_t ID);

    static bool isHost();
    static void setHosting(bool hosting);

    static bool isFocused;

    static std::string playerNames[MAX_PLAYERS];
    static int32_t playerIDs[MAX_PLAYERS];
    static sf::Color playerColors[MAX_PLAYERS];
    static uint8_t numberOfPlayers;
    static uint8_t playerRankings[MAX_PLAYERS];

    static sf::Font globalFont;
    static sf::Texture playerBase;
    static sf::Texture playerShading;
    static sf::Texture playerHat;
    static sf::Texture enemyHat;
    static sf::Texture iceblock;

    static sf::Texture fireballTexture;
    static sf::Texture iceballTexture;

private:
    static TCPSocket socket;
    static std::string playerName;
    static int32_t playerID;
    static std::string gameName;
    static int32_t gameID;
    static bool isHosting;

    GameScene* scenes[GameState::STATE_COUNT];
    GameScene* currentScene;
    GameState currentState;
    sf::Clock clock;
    std::vector<sf::Event> polledEvents;
};

#endif // GAME_MANAGER_H
