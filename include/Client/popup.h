#ifndef POPUP_H
#define POPUP_H

#include <SFML/Graphics.hpp>
#include <string>

const sf::Vector2f PopupWindowSize = sf::Vector2f(640, 400);

class Popup
{
public:
    enum PopupType
    {
        OK,
        YESNO
    };

    Popup(sf::Vector2i ratio);

    //display this popup with the given type and message
    void show(PopupType, std::string message);

    void resize(sf::Vector2i ratio);

    /**
     * Returns true while the window is still displaying, false once the user selects an option
     * Use the returnValue variable to see what the user selected.
     * YES/OK = true, NO = false
     */
    bool update(sf::Vector2f mousePos, bool& returnValue);

    void draw(sf::RenderWindow& window);

private:
    sf::Font m_font;
    sf::Text displayedText;
    sf::Texture backgroundTexture;
    sf::Sprite backgroundSprite;
    sf::Text OKText;
    sf::Text YESText;
    sf::Text NOText;
    PopupType type;
};


#endif //POPUP_H