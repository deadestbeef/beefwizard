#ifndef GAME_STATE_H
#define GAME_STATE_H

/**
 * This defines all the possible states the game could be in. Each state is
 * accompanied by a scene that will be displayed when the game in in that state.
 *
 * This is an enum and not enum class because casting is lame.
 */
enum GameState
{
    LOGIN_STATE = 0,
    MENU_STATE,
    LOBBY_STATE,
    GAME_SEARCH_STATE,
    INGAME_STATE,
    POSTGAME_STATE,
    LEADERBOARD_STATE,
    TUTORIAL_STATE,
    STATE_COUNT
};


#endif //GAME_STATE_H