
#ifndef LEADERBOARD_H
#define LEADERBOARD_H

#include "game_scene.h"
#include <cstdint>
#include <vector>

struct ScoreEntry
{
    std::string Name;
    int NumberOfWins;
    int GamesPlayed;
    float AveragePlacement;
};


class Leaderboard : public GameScene
{

public:
    Leaderboard(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void exit() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

private:
    static const int ENTRIES_PER_PAGE = 10;
    static const int COLUMNS = 4;

    bool getScores();

    sf::Text header[COLUMNS];
    sf::Text listedScores[ENTRIES_PER_PAGE][COLUMNS];

    sf::Vector2i aspect_ratio;
    sf::Font m_font;
    sf::Text leaderboardText;
    int32_t numberOfScores;
    std::vector<ScoreEntry> playerScoreList;

    sf::Texture backButtonTexture;
    sf::Sprite backButtonSprite;


    bool scoresFound;
};

#endif //LEADERBOARD_H