
#ifndef GAME_SEARCH_H
#define GAME_SEARCH_H

#include "game_scene.h"
#include "popup.h"
#include <cstdint>

/**
 * MainMenu is the menu the player sees after they log in.
 */
class GameSearch: public GameScene
{

public:
    GameSearch(sf::Vector2i ratio);

    virtual void reset() override;

    virtual void resize(sf::Vector2i ratio) override;

    virtual void exit() override;

    virtual GameState update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed) override;

    virtual void draw(sf::RenderWindow& window) override;

    private:

    static const int GAMES_PER_PAGE = 10;

    bool gameSearch();
    bool joinGame(int32_t gameID, std::string gameName);

    sf::Text listedGames[GAMES_PER_PAGE];
    sf::Text gameCapacities[GAMES_PER_PAGE];

    sf::Vector2i aspect_ratio;
    sf::Font m_font;
    sf::Text findGamesText;
    int32_t numberOfGames;
    std::string* gameNameList;
    int32_t* gameIDList;

    std::vector<int> gameNumbers;

    sf::Texture backButtonTexture;
    sf::Sprite backButtonSprite;


    bool gamesFound;

    Popup popup;
    bool displayingPopup;
};

#endif //GAME_SEARCH_H