#pragma once // :)

#include <string>
#include <SFML/Network.hpp>

// Extra beefy port
const uint16_t BEEFNET_PORT = 0xBEEF;

// Probably some other defines here depending upon
// how we define the protocols

class TCPSocket
{
public:
    TCPSocket();
    TCPSocket(const std::string& serverName, uint16_t portNumber = BEEFNET_PORT);
    ~TCPSocket();

    void Init();

    int ConnectToServer(const std::string& serverName, uint16_t portNumber = BEEFNET_PORT);
    //void WrapSocketFD(int sfd);
    bool IsGood() const;

    //bool HasData();
    bool Read(void* buffer, int numBytes);
    bool ReadSync(void* buffer, int numBytes);
    bool WriteSync(const void* data, int numBytes);

    std::string ReadString();
    bool WriteString(const std::string& str);

    void CloseSocket();

    // Maybe later
    // int Read_Async(int socketFD, void* buffer, int numBytes);
    // int Write_Async(int socketFD, void* data, int numBytes);

    sf::TcpSocket m_socket;
private:
    // That's some dead beef you got there
    int state = 0xDEAD;
    //int socketFD;

};


class TCPListener
{
public:
    TCPListener(uint16_t portNumber = BEEFNET_PORT);
    ~TCPListener();

    int InitListener(uint16_t portNumber = BEEFNET_PORT);

    //bool HasNewConnection();
    bool AcceptConnection(TCPSocket& sock);

    bool IsGood() const;
    void CloseSocket();

private:
    int state = 0xDEAD;
    int socketFD;
    sf::TcpListener m_listener;
};
