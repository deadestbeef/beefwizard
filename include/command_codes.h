#ifndef COMMAND_CODES_H
#define COMMAND_CODES_H

/**
 * Defines constants for readbility and whatnot.
 *
 * These are not set in stone, and may change to improce clarity.
 */
enum CommandCodes
{
    ERROR = 0,
    REGISTER,
    CREATE_GAME,
    LIST_GAMES,
    JOIN_GAME,
    PLAYER_JOIN_GAME,
    PLAYER_LEAVE_GAME,
    LEAVE_GAME,
    LOAD_GAME, //instead of Begin?
    LOGOUT,
    CHAT,
    LEADERBOARD,
    START_GAME, //instead of match start?
    STATE_UPDATE,
    PROJ_NOTIFICATION,
    COLLISION_NOTIFICATION,
    GAME_FINISHED
};


#endif //COMMAND_CODES_H