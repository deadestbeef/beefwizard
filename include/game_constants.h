#ifndef GAME_CONSTANTS_H
#define GAME_CONSTANTS_H

#include <cstdint>

#define MAX_PLAYERS 6
#define FIREBALL_LIFESPAN 4
#define ICEBALL_LIFESPAN 6
#define ICEBALL_FREEZE_DURATION 3
#define PLAYER_STARTING_HP 100

enum class PlayerStatus : uint8_t
{
    Alive, Dead, Disconnected
};

enum class ProjectileTypes : uint8_t
{
    Fireball, Iceball
};

#endif //GAME_CONSTANTS_H
