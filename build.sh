###################################################################################################
# Flags:
#       -g                  Build with debug flags
#       -p, --prototype     Build only the prototype game (does not build by default)
#       -c, --client        Build only the client (client and server build by default)
#       -s, --server        Build only the server (client and server build by default)
#       --rebuild           Remove all generated object files and executables before building
#
# Notes:
#           This script is smart enough to not rebuild any object files whose .cpp files have
#           not been modified, but it is not smart enough to parse those files to determine
#           if .h file dependencies have been modified. If you edit a header file and need to
#           rebuild, use the --rebuild flag.
#
###################################################################################################
#!/bin/bash

UPTODATE=true

DEBUG_MODE=false
CLIENT_ONLY=false
SERVER_ONLY=false

mkdir -p obj
mkdir -p bin

for i in "$@"
do
case $i in
    -g)
    DEBUG_MODE=true
    shift # past argument=value
    ;;
    -c*|--client)
    CLIENT_ONLY=true
    shift # past argument=value
    ;;
    -s*|--server)
    SERVER_ONLY=true
    shift # past argument=value
    ;;
    --rebuild)
    rm -r obj/*
    rm -r bin/*
    shift
    ;;
    *)
          # unknown option
    ;;
esac
done

mkdir -p obj/Client
mkdir -p obj/Server

CLIENT=true
SERVER=true

CLIENT=true
SERVER=true
if $CLIENT_ONLY && $SERVER_ONLY; then
    CLIENT=true
    SERVER=true
elif $CLIENT_ONLY; then
    SERVER=false
elif $SERVER_ONLY; then
    CLIENT=false
fi


for f in src/*.cpp
do
    FILE=${f%.*}
    FILE=${FILE##*/}
    if [ ${f} -nt obj/${FILE}.o ]; then
        echo "Building $FILE..."
        if $DEBUG_MODE; then
            g++ -c -g -std=c++11 -I include $f -o obj/$FILE.o -Wall
        else
            g++ -c -O3 -std=c++11 -I include $f -o obj/$FILE.o -Wall
        fi
        UPTODATE=false
    fi
done

if $CLIENT; then
    for f in src/Client/*.cpp
    do
        FILE=${f%.*}
        FILE=${FILE##*/}
        if [ ${f} -nt obj/Client/${FILE}.o ]; then
            echo "Building $FILE..."
            if $DEBUG_MODE; then
                g++ -c -g -std=c++11 -I include/Client -I include  $f -o obj/Client/$FILE.o -Wall
            else
                g++ -c -O3 -std=c++11 -I include/Client -I include  $f -o obj/Client/$FILE.o -Wall
            fi
            UPTODATE=false
        fi
    done

    if $UPTODATE; then
        echo "Client up to date."
    else
        echo "Linking Client..."
        if $DEBUG_MODE; then
            g++ -g obj/*.o obj/Client/*.o -o bin/BeefClient -lsfml-graphics -lsfml-window -lsfml-network -lsfml-system
        else
            g++ -O3 obj/*.o obj/Client/*.o -o bin/BeefClient -lsfml-graphics -lsfml-window -lsfml-network -lsfml-system
        fi
    fi
fi

if $SERVER; then
    for f in src/Server/*.cpp
    do
        FILE=${f%.*}
        FILE=${FILE##*/}
        if [ ${f} -nt obj/Server/${FILE}.o ]; then
            echo "Building $FILE..."
            if $DEBUG_MODE; then
                g++ -c -g -std=c++11 -I include/Server -I include  $f -o obj/Server/$FILE.o -Wall
            else
                g++ -c -O3 -std=c++11 -I include/Server -I include  $f -o obj/Server/$FILE.o -Wall
            fi
            UPTODATE=false
        fi
    done

    if $UPTODATE; then
        echo "Server up to date."
    else
        echo "Linking Server..."
        if $DEBUG_MODE; then
            g++ -g obj/*.o obj/Server/*.o -o bin/BeefServer -lsfml-network -lsfml-system
        else
            g++ -O3 obj/*.o obj/Server/*.o -o bin/BeefServer -lsfml-network -lsfml-system
        fi
    fi
fi
