#include "connections.h"

// My wall o includes
#include <iostream>
#include <cassert>

TCPSocket::TCPSocket()
{

}

TCPSocket::TCPSocket(const std::string& serverName, uint16_t portNumber)
{
    state = ConnectToServer(serverName, portNumber);
}

TCPSocket::~TCPSocket()
{
    //this actually closes the socket on a copy
    //CloseSocket();
}

int TCPSocket::ConnectToServer(const std::string& serverName, uint16_t portNumber)
{
    /*
    // Get server host info
    hostent* host = gethostbyname(serverName.c_str()); // Note: Deprecated call, living life on the edge.

    // Prepare the sockaddr that will be used to connect to the server
    sockaddr_in sendSockAddr;
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr));
    sendSockAddr.sin_family      = AF_INET; // Address Family Internet
    sendSockAddr.sin_addr.s_addr = inet_addr(inet_ntoa(*(in_addr*)*host->h_addr_list));
    sendSockAddr.sin_port        = htons(portNumber);

    //don't shadow local variables, Cameron!
    socketFD = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFD < 0)
    {
        perror("Socket Creation Error");
        state = -1;
        return -1;
    }
    if (connect(socketFD, (sockaddr*)&sendSockAddr, sizeof(sendSockAddr)) < 0)
    {
        perror("Socket Connection Error");
        close(socketFD);
        socketFD = -1;
        state = -1;
        return -1;
    }
    */

    sf::IpAddress serverAddress(serverName);
    auto status = m_socket.connect(serverAddress, portNumber);

    state = (status == sf::Socket::Error) ? -1 : 0;
    return state;
}

void TCPSocket::Init()
{
    state = 0;
}

//void TCPSocket::WrapSocketFD(int sfd)
//{
//    socketFD = sfd;
//    state = 0;
//}

bool TCPSocket::IsGood() const
{
    return (state == 0); //(socketFD > 0);// && );
}

/*
bool TCPSocket::HasData()
{
    assert(state == 0);// && socketFD >= 0);

    struct pollfd pfd;
    pfd.fd = socketFD;
    pfd.events = POLLRDNORM;

    //returns immediately
    return poll( &pfd, 1, 0 )?true:false;
}
*/

bool TCPSocket::Read(void* buffer, int numBytes)
{
    m_socket.setBlocking(false);
    auto rVal = ReadSync(buffer, numBytes);
    m_socket.setBlocking(true);

    return rVal;
}

bool TCPSocket::ReadSync(void* buf, int numBytes)
{
    uint8_t* buffer = reinterpret_cast<uint8_t*>(buf);

    if (state == 0xDEAD)
    {
        std::cerr << "ERROR: Attempted to read from a 0xDEAD socket." << std::endl;
        return false;
    }

    assert(state == 0);// && socketFD >= 0);

    int bytesRead = 0;
    while (bytesRead < numBytes)
    {
        std::size_t bRead;
        auto status = m_socket.receive(&buffer[bytesRead], numBytes - bytesRead, bRead);
        /*
        int ret = read(socketFD, &reinterpret_cast<char*>(buffer)[bytesRead], numBytes - bytesRead);

        // I don't know how we want to handle errors on read, but for now lets just assume they work.
        // I make no guarentees about state if you manage to penetrate these if statements :)
        if (ret < 0)
        {
            std::cerr << "ERROR: Read from socket failed before all data has been retrieved...\n"
                << "Bytes Read: " << bytesRead << "\nerrno: " << errno << std::endl;
            close(socketFD);
            state = 0xDEAD;
            return false;
        }
        else if (ret == 0)
        {
            std::cerr << "Closed socket detected." << std::endl;

            state = 0xDEAD;
            return false;
        }*/

        if(status == sf::Socket::Error)
        {
            std::cerr << "ERROR: Read from socket failed before all data has been retrieved...\n"
                << "Bytes Read: " << bytesRead << std::endl;
            //m_socket.close();
            state = 0xDEAD;
            return false;
        }
        else if(status == sf::Socket::Disconnected)
        {
            std::cerr << "Closed socket detected." << std::endl;

            state = 0xDEAD;
            return false;
        }
        else if (status == sf::Socket::NotReady)//non-blocking read
        {
            return false;
        }

        bytesRead += bRead;
        buffer += bytesRead;
    }

    return true;
}

bool TCPSocket::WriteSync(const void* data, int numBytes)
{
    if (state == 0xDEAD)
    {
        std::cerr << "ERROR: Attempted to write to a 0xDEAD socket." << std::endl;
        return false;
    }

    assert(state == 0);// && socketFD >= 0);

    auto status = m_socket.send(data, numBytes);
    return (status == sf::Socket::Done);

    /*
    int ret = write(socketFD, data, numBytes);
    if (ret < 0)
    {
        std::cerr << "ERROR: Failed to write to socket...\n"
            << "\nerrno: " << errno << std::endl;
        close(socketFD);
        return false;
    }

    return true;
    */
}

std::string TCPSocket::ReadString()
{
    uint16_t string_size;
    if (!ReadSync(&string_size, 2))
    {
        std::cout << "Socket died while reading a string size." << std::endl;
        return "0xDEADBEEF";
    }

    char name_buffer[string_size];
    if (!ReadSync(name_buffer, string_size))
    {
        std::cout << "Socket died while reading a string." << std::endl;
        return "0xDEADBEEF";
    }

    return std::string(name_buffer, string_size);
}

bool TCPSocket::WriteString(const std::string& str)
{
    assert(str.size() < UINT16_MAX);
    uint16_t size = str.size();

    if (!WriteSync(&size, 2))
    {
        std::cerr << "Socket died while writing a string." << std::endl;
        return false;
    }

    if (!WriteSync(str.c_str(), str.size()))
    {
        std::cerr << "Socket died while writing a string." << std::endl;
        return false;
    }
    return true;
}

void TCPSocket::CloseSocket()
{
    //close(socketFD);
    //socketFD = 0xDEAD;
    m_socket.disconnect();
    state = 0xDEAD;
}



TCPListener::TCPListener(uint16_t portNumber)
{
    state = InitListener(portNumber);
}

TCPListener::~TCPListener()
{
    CloseSocket();
}

int TCPListener::InitListener(uint16_t portNumber)
{
    /*
    // Prepare the sockaddr that will be used to listen for connections
    sockaddr_in acceptSockAddr;
    bzero((char*)&acceptSockAddr, sizeof(acceptSockAddr));
    acceptSockAddr.sin_family      = AF_INET; // Address Family Internet
    acceptSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    acceptSockAddr.sin_port        = htons(portNumber);

    //don't shadow local variables, Cameron!
    socketFD = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFD < 0)
    {
        std::cerr << "Socket creation for server failed with: " << errno << std::endl;
        state = -1;
        return -1;
    }

    const int on = 1;
    setsockopt(socketFD, SOL_SOCKET, SO_REUSEADDR, (char*)&on,
                sizeof(int));


    // TODO: Some potential error instrumenting for these
    bind(socketFD, (sockaddr*)&acceptSockAddr, sizeof(acceptSockAddr));

    listen(socketFD, 100);
    */


    auto returnVal = (m_listener.listen(portNumber) == sf::Socket::Done)?0:-1;
    m_listener.setBlocking(false);



    return returnVal;
}
/*
bool TCPListener::HasNewConnection()
{
    struct pollfd pfd;
    pfd.fd = socketFD;
    pfd.events = POLLRDNORM;

    //returns immediately
    return poll( &pfd, 1, 0 ) ? true : false;
}
*/
bool TCPListener::AcceptConnection(TCPSocket& sock)
{
    //assert(state == 0 && socketFD >= 0);

    auto status = m_listener.accept(sock.m_socket);

    if (status == sf::Socket::Done)
    {
        sock.Init();
        return true;
    }

    return false;


    /*

    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof(newSockAddr);

    int newSd = accept(socketFD, (sockaddr*)&newSockAddr, &newSockAddrSize);
    sock.WrapSocketFD(newSd);
    //return sock;
    */
}

bool TCPListener::IsGood() const
{
    if (state == 0)// && socketFD >= 0)
        return true;

    return false;
}

void TCPListener::CloseSocket()
{

    m_listener.close();

    /*
    close(socketFD);
    socketFD = 0xDEAD;
    state = 0xDEAD;
    */
}
