#include "beef_server.h"
#include "command_codes.h"
#include "game_constants.h"
#include <list>
#include <iostream>
#include <cstdint>
#include <cstring>
#include <cassert>

using std::cout;
using std::endl;

BeefServer::~BeefServer()
{
    cout << "Server shutting down. Closing all open TCP connections." << endl;
    for (auto& connection : open_tcp_connections)
    {
        connection->CloseSocket();
    }

    for (auto& player : players)
    {
        player.socket->CloseSocket();
    }
}

void BeefServer::Update()
{
    while (true)
    {
        std::shared_ptr<TCPSocket> new_connection = std::make_shared<TCPSocket>();
        if(tcp_listener.AcceptConnection(*new_connection))
        {
            //for debugging purposes early on
            std::cout << "Accepting new player" << std::endl;
            open_tcp_connections.push_back(new_connection);
        }
        else
            break;
    }

    for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
    {
        PlayerData& player = *iterator;
        if (player.socket->IsGood())
        {
            uint8_t code;
            while(player.socket->Read(&code, 1))
            {
                switch(code)
                {
                    case REGISTER:
                    {
                        std::cout << "Registering a user!" << std::endl;
                        registerUser(player.socket);
                        break;
                    }
                    case CREATE_GAME:
                    {
                        createGame(player);
                        break;
                    }
                    case LIST_GAMES:
                    {
                        listGame(player);
                        break;
                    }
                    case JOIN_GAME:
                    {
                        //this also notifies other players that you joined
                        joinGame(player);
                        break;
                    }
                    case LEAVE_GAME:
                    {
                        //this also notifies other players that you left
                        exitGame(player);
                        break;
                    }
                    case LOAD_GAME:
                    {
                        beginGame(player);
                        break;
                    }
                    case LOGOUT:
                    {
                        unregister(player);
                        break;
                    }
                    case CHAT:
                    {
                        chat(player);
                        break;
                    }
                    case LEADERBOARD:
                    {
                        scoreboardRequest(player);
                        break;
                    }
                    case START_GAME:
                    {
                        //this is when clients say they are ready
                        notifyLoaded(player);
                        break;
                    }
                    case STATE_UPDATE:
                    {
                        stateUpdate(player);
                        break;
                    }
                    case PROJ_NOTIFICATION:
                    {
                        newProjectile(player);
                        break;
                    }
                    case GAME_FINISHED:
                    {
                        gameFinalization(player);
                        break;
                    }
                    default:
                    {
                        std::cout << "Unknown Command Code " << std::to_string(code) << std::endl;
                    }
                }
            }
        }
        else
        {
            player.socket->CloseSocket();
            std::cout << "Player disconnected from the server: " << player.name << std::endl;
            for (auto server_iterator = game_servers.begin(); server_iterator != game_servers.end(); ++server_iterator)
            {
                if (server_iterator->GetHostID() == player.player_id)
                {
                    if (!server_iterator->ExitGame(player, false))
                    {
                        cout << "Shutting down server [" << server_iterator->GetServerID() << "]." << endl;
                        game_servers.erase(server_iterator--);
                    }
                }
            }
            std::cout << "Erasing player" << std::endl;
            players.erase(iterator--);
        }
    }

    for (auto iterator = open_tcp_connections.begin(); iterator != open_tcp_connections.end(); ++iterator)
    {
        auto& connection = *iterator;
        if (connection->IsGood())
        {
            while (true)
            {
                uint8_t code;
                if (!connection->Read(&code, 1))
                {
                    if(!connection->IsGood())
                    {
                        connection->CloseSocket();
                        std::cout << "Client leaving" << std::endl;
                        open_tcp_connections.erase(iterator--);
                    }
                    break;
                }

                if (code == REGISTER)
                {
                    if (registerUser(connection))
                    {
                        open_tcp_connections.erase(iterator--);
                    }
                }
                else
                {
                    cout << "Sending error message: [" << GetErrorString(Error::NotLoggedIn) << "]" << endl;

                    uint8_t message[3];
                    message[0] = static_cast<uint8_t>(ERROR);
                    uint16_t error = static_cast<uint16_t>(Error::NotLoggedIn);
                    std::memcpy(&message[1], &error, 2);
                    if (!connection->WriteSync(message, 3))
                    {
                        open_tcp_connections.erase(iterator--);
                    }
                }
            }
        }
        else
        {
            connection->CloseSocket();
            std::cout << "Client leaving" << std::endl;
            open_tcp_connections.erase(iterator--);
        }
    }

    for (auto game_iterator = game_servers.begin(); game_iterator != game_servers.end(); ++game_iterator)
    {
        if (game_iterator->Update() == GameState::PostGame)
        {
            cout << "Finalizing game [" << game_iterator->GetServerID() << "]" << endl;
            auto final_player_states = game_iterator->GetPlayers();
            scoreboard.AddRecords(final_player_states);
            game_servers.erase(game_iterator--);
        }
    }
}

/**************************************************************************************************
 * Method:          register_user()
 * Parameters:      TCPSocket socket
 *                      The socket object from which the communication came in on
 *
 * Purpose:         Registers new users with the BeefServer. If the requested name is not available,
 *                  an error will be sent. Otherwise, the server will inform the new user with their
 *                  player ID.
 *
 * Preconditions:   The socket object has already read the [messageID] field.
 *
 *************************************************************************************************/
bool BeefServer::registerUser(std::shared_ptr<TCPSocket>& socket)
{
    std::string player_name = socket->ReadString();

    std::cout << "Registration request for user \"" << player_name << "\"" << std::endl;

    for (PlayerData& data : players)
    {
        if (data.name == player_name)
        {
            // Send error message
            std::cout << "Registration failed. Name taken." << std::endl;
            sendError(socket, Error::NameNotAvailable);
            return false;
        }
    }

    int id = (players.size() > 0) ? players[players.size() - 1].player_id + 1 : 0;
    PlayerData new_player(player_name, socket, id);

    players.push_back(new_player);

    uint8_t confirmation_message[5];
    confirmation_message[0] = static_cast<uint8_t>(REGISTER);
    std::memcpy(&confirmation_message[1], &new_player.player_id, 4);

    if (!new_player.socket->WriteSync(confirmation_message, 5))
    {
        return false;
    }

    return true;
}

void BeefServer::createGame(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        // Socket was closed; do not create game.
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" is attempting to create a game." << std::endl;

    for (auto iter = game_servers.begin(); iter != game_servers.end(); ++iter)
    {
        if (iter->GetHostID() == player_id)
        {
            // Whoa! Not good.
            std::cout << "ERROR: Player has another hosted session..." << std::endl;
            sendError(player, Error::AlreadyHosting);
            return;
        }
    }

    int id = -1;
    if (game_servers.size())
        id = game_servers[game_servers.size() - 1].GetServerID() + 1;
    else
        id = 0;

    uint8_t confirmation_message[5];
    confirmation_message[0] = static_cast<uint8_t>(CREATE_GAME);
    std::memcpy(&confirmation_message[1], &id, 4);

    if (!player.socket->WriteSync(confirmation_message, 5))
    {
        return;
    }

    game_servers.push_back(GameServer(id, player));
}

void BeefServer::listGame(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        // Socket was closed; do not list games.
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" requesting game list." << std::endl;

    int num_games = 0;
    for (auto &server : game_servers)
    {
        if (server.GetNumPlayers() < MAX_PLAYERS && server.GetState() == GameState::Lobby)
        {
            ++num_games;
        }
    }

    uint8_t header[5];
    header[0] = static_cast<uint8_t>(LIST_GAMES);
    std::memcpy(&header[1], &num_games, 4);

    if (!player.socket->WriteSync(header, 5))
    {
        // Socket was closed; do not list games.
        return;
    }

    for (auto iter = game_servers.begin(); iter != game_servers.end(); ++iter)
    {
        if (iter->GetNumPlayers() < MAX_PLAYERS && iter->GetState() == GameState::Lobby)
        {
            std::string game_name = iter->GetName();
            uint16_t size = game_name.size();
            int game_id = iter->GetServerID();
            uint8_t num_players = static_cast<uint8_t>(iter->GetNumPlayers());

            if (!player.socket->WriteSync(&size, sizeof(size)))
            {
                // Socket was closed; do not list games.
                return;
            }

            if (!player.socket->WriteSync(game_name.c_str(), game_name.size()))
            {
                // Socket was closed; do not list games.
                return;
            }

            if (!player.socket->WriteSync(&game_id, 4))
            {
                // Socket was closed; do not list games.
                return;
            }

            if (!player.socket->WriteSync(&num_players, 1))
            {
                return;
            }
        }
    }
}

void BeefServer::joinGame(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        // Socket was closed; do not join game.
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" requesting to JOIN a game." << std::endl;

    int32_t game_id;
    if (!player.socket->ReadSync(&game_id, 4))
    {
        // Socket was closed; do not join game.
        return;
    }

    for (auto iter = game_servers.begin(); iter != game_servers.end(); ++iter)
    {
        if (iter->GetServerID() == game_id)
        {
            iter->JoinGame(player);
            return;
        }
    }

    std::cout << "Game ID not found for JOIN request." << std::endl;
    sendError(player, Error::GameNotFound);
}

void BeefServer::exitGame(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" requesting to EXIT a game." << std::endl;

    int32_t game_id;
    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    for (auto iter = game_servers.begin(); iter != game_servers.end(); ++iter)
    {
        if (iter->GetServerID() == game_id)
        {
            if (!(iter->ExitGame(player)))
            {
                cout << "Shutting down server [" << iter->GetServerID() << "]." << endl;
                game_servers.erase(iter--);
            }

            return;
        }
    }

    std::cout << "Game ID not found for EXIT request." << std::endl;
    sendError(player, Error::GameNotFound);
}

void BeefServer::beginGame(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" requesting to BEGIN a game." << std::endl;

    int32_t game_id;
    player.socket->ReadSync(&game_id, 4);
    for (auto iter = game_servers.begin(); iter != game_servers.end(); ++iter)
    {
        if (iter->GetServerID() == game_id)
        {
            iter->BeginGame(player);
            return;
        }
    }

    std::cout << "Game ID not found for BEGIN request." << std::endl;
    sendError(player, Error::GameNotFound);
}

void BeefServer::unregister(PlayerData& player)
{
    // Read the id since we are on TCP and already have the player...
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    assert(player.player_id == player_id);

    std::cout << "Player \"" << player.name << "\" is unregistering." << std::endl;
    for (auto iter = players.begin(); iter != players.end(); ++iter)
    {
        if (iter->player_id == player_id)
        {
            // I suspect it will break if we remove the player_data first,
            // so send the reply then remove the player.
            uint8_t reply[5];
            reply[0] = static_cast<uint8_t>(LOGOUT);
            std::memcpy(&reply[1], &player_id, 4);

            if (!player.socket->WriteSync(reply, 5))
            {
                // Don't need to drop player because player is being dropped anyway
            }

            players.erase(iter--);
            break;
        }
    }
}

void BeefServer::chat(PlayerData& player)
{
    int32_t player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
       return;
    }

    assert(player.player_id == player_id);

    int32_t game_id;
    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    for (auto &server : game_servers)
    {
        if (server.GetServerID() == game_id)
        {
            server.Chat(player);
            return;
        }
    }

    cout << "Sending error message: [" << GetErrorString(Error::CannotChatServerDoesNotExist);
    cout << "] to player: " << player.name << endl;
    sendError(player, Error::CannotChatServerDoesNotExist);
}

void BeefServer::scoreboardRequest(PlayerData& player)
{
    int player_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    assert(player_id == player.player_id);

    auto records = scoreboard.GetRecordsByRank();

    int message_size = 5;
    for (auto& record : records)
    {
        message_size += (2 + record.GetName().size());
        message_size += 4 + 4 + 4;
    }

    uint8_t message[message_size];
    int index = 0;
    message[index++] = static_cast<uint8_t>(LEADERBOARD);
    int32_t num_records = records.size();
    std::memcpy(&message[index], &num_records, 4);
    index += 4;

    for (auto& record : records)
    {
        uint16_t name_size = static_cast<uint16_t>(record.GetName().size());
        std::memcpy(&message[index], &name_size, 2);
        index += 2;
        std::memcpy(&message[index], record.GetName().c_str(), name_size);
        index += name_size;
        int32_t num_wins = record.GetNumWins();
        std::memcpy(&message[index], &num_wins, 4);
        index += 4;
        int32_t games_played = record.GetNumGames();
        std::memcpy(&message[index], &games_played, 4);
        index += 4;
        float average_rank = record.GetAverageRank();
        std::memcpy(&message[index], &average_rank, 4);
        index += 4;
    }

    cout << "Sending leaderboard information to player [" << player.name << "]" << endl;
    if (!player.socket->WriteSync(message, message_size))
    {
        return;
    }
}

void BeefServer::notifyLoaded(PlayerData& player)
{
    int32_t player_id;
    int32_t game_id;

    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    assert(player_id == player.player_id);

    for (auto &game : game_servers)
    {
        if (game.GetServerID() == game_id)
        {
            game.NotifyLoaded(player.player_id);
            break;
        }
    }
}

void BeefServer::stateUpdate(PlayerData& player)
{
    int32_t game_id;
    int32_t player_id;
    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    assert(player_id == player.player_id);

    for (auto &game : game_servers)
    {
        if (game.GetServerID() == game_id)
        {
            game.UpdateState(player);
            break;
        }
    }
}

void BeefServer::newProjectile(PlayerData& player)
{
    int player_id;
    int game_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    ProjectileData projectile;
    if (!player.socket->ReadSync(&projectile.type, 1))
    {
        return;
    }

    if (!player.socket->ReadSync(&projectile.orientation, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&projectile.velocity.x, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&projectile.velocity.y, 4))
    {
        return;
    }


    for (auto &server : game_servers)
    {
        if (server.GetServerID() == game_id)
        {
            server.CreateProjectile(projectile, player_id);
            return;
        }
    }

    cout << "Sending error message: [" << GetErrorString(Error::CannotCreateProjectileGameDoesNotExist);
    cout << "] to player: " << player.name << endl;
    sendError(player, Error::CannotCreateProjectileGameDoesNotExist);
}

void BeefServer::gameFinalization(PlayerData& player)
{
    int player_id;
    int game_id;
    if (!player.socket->ReadSync(&player_id, 4))
    {
        return;
    }

    if (!player.socket->ReadSync(&game_id, 4))
    {
        return;
    }

    for (auto &server : game_servers)
    {
        if (server.GetServerID() == game_id)
        {
            server.FinalizeACK(player_id);
            return;
        }
    }

}

bool BeefServer::sendError(PlayerData& player, Error e)
{
   return sendError(player.socket, e);
}

bool BeefServer::sendError(std::shared_ptr<TCPSocket>& socket, Error e)
{
    uint8_t message[3];
    message[0] = static_cast<uint8_t>(ERROR);
    uint16_t error = static_cast<uint16_t>(e);
    std::memcpy(&message[1], &error, 2);
    if (socket->WriteSync(message, 3) <= 0)
    {
        std::cerr << "Something went wrong writing to the socket in sendError" << std::endl;
        return false;
    }
    return true;
}
