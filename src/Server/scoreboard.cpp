#include "scoreboard.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>

using std::cout;
using std::endl;

PlayerScore::PlayerScore(std::string name) : name{name}, num_wins{0}
{
}

void PlayerScore::AddRank(int rank)
{
    rankings.push_back(rank);
    if (rank == 1)
    {
        ++num_wins;
    }
}

void PlayerScore::AddSurvivalTime(float time)
{
    survival_times.push_back(time);
}

std::string PlayerScore::GetName()
{
    return name;
}

int PlayerScore::GetNumGames()
{
    return rankings.size();
}

float PlayerScore::GetAverageRank() const
{
    float mean = 0;
    for (auto rank : rankings)
    {
        mean += rank;
    }
    return mean / rankings.size();
}

float PlayerScore::GetAverageTime() const
{
    float mean = 0;
    for (auto t : survival_times)
    {
        mean += t;
    }
    return mean / survival_times.size();
}

int PlayerScore::GetNumWins()
{
    return num_wins;
}

const std::vector<int>& PlayerScore::GetRankings()
{
    return rankings;
}

const std::vector<float>& PlayerScore::GetSurvivalTimes()
{
    return survival_times;
}


Scoreboard::Scoreboard()
{
    std::ifstream ifile("Scoreboard.bxt");
    if (!ifile)
        return;

    std::string line;
    while (std::getline(ifile, line, '\n'))
    {
        std::stringstream line_stream(line);
        std::string substr;


        std::getline(line_stream, substr, ',');
        PlayerScore player(substr);

        std::getline(line_stream, substr, ',');
        int num_rankings = stoi(substr);
        int i = 0;
        while (i < num_rankings && std::getline(line_stream, substr, ','))
        {
            ++i;

            int ranking = stoi(substr);
            player.AddRank(ranking);
        }

        std::getline(line_stream, substr, ',');
        int num_times = stoi(substr);
        i = 0;
        while (i < num_times && std::getline(line_stream, substr, ','))
        {
            ++i;

            int time = stof(substr);
            player.AddSurvivalTime(time);
        }

        player_records.push_back(player);
    }

    ifile.close();
}

Scoreboard::~Scoreboard()
{
    std::ofstream ofile("Scoreboard.bxt");
    if (!ofile)
        return;

    for (auto& score : player_records)
    {
        ofile << score.GetName() << ", ";
        ofile << score.GetRankings().size() << ", ";
        for (auto rank : score.GetRankings())
        {
            ofile << rank << ", ";
        }

        ofile << score.GetSurvivalTimes().size() << ", ";
        for (auto time : score.GetSurvivalTimes())
        {
            ofile << time << ", ";
        }

        ofile << "\n";
    }

    ofile.close();
}

void Scoreboard::AddRecords(std::vector<PlayerData>& players)
{
    for (auto player : players)
    {
        bool found = false;
        for (auto& record : player_records)
        {
            if (record.GetName() == player.name)
            {
                found = true;
                record.AddRank(player.rank);
                record.AddSurvivalTime(player.survival_time);
            }
        }

        if (!found)
        {
            PlayerScore record{player.name};
            record.AddRank(player.rank);
            record.AddSurvivalTime(player.survival_time);
            player_records.push_back(record);
        }
    }
}

std::vector<PlayerScore>& Scoreboard::GetRecordsByRank()
{
    std::sort(player_records.begin(), player_records.end(),
        [](const PlayerScore& a, const PlayerScore& b)
    {
        return a.GetAverageRank() < b.GetAverageRank();
    });

    return player_records;
}

std::vector<PlayerScore>& Scoreboard::GetRecordsByTime()
{
    std::sort(player_records.begin(), player_records.end(),
        [](const PlayerScore& a, const PlayerScore& b)
    {
        return a.GetAverageTime() < b.GetAverageTime();
    });

    return player_records;
}
