#include "game_server.h"
#include "command_codes.h"
#include "colors.h"
#include "error_codes.h"
#include "game_constants.h"
#include <cmath>
#include <cstring>
#include <algorithm>
#include <iostream>

#define MATCH_START_TIMEOUT 10 //seconds
#define PLAYER_RADIUS 64
#define ISLAND_SIZE 1500 // Starting island size, radius, pixels
#define ISLAND_SHRINK_RATE 15 // Pixels-per-second
#define LAVA_DAMAGE 10 // Damage-per-second
#define SERVER_MIN_TICK_RATE 50 // Hertz
#define FRICTION 125 // pixels-per-second-per-second

#define FIREBALL_SAFETY 0.35
#define FIREBALL_RADIUS 24
#define FIREBALL_KNOCKBACK 0.7
#define FIREBALL_DAMAGE 10

#define ICEBALL_SAFETY 0.75
#define ICEBALL_RADIUS 48
#define ICEBALL_KNOCKBACK 0.2
#define ICEBALL_DAMAGE 10

using std::cout;
using std::endl;

GameServer::GameServer(int id, PlayerData player) : id{id}, state{GameState::Lobby},
    clock{}, start_clock{}, hosting_player{player.player_id}, island_radius{ISLAND_SIZE},
    num_collisions{0}, player_status{}, OKToSend{false}
{
    player.game_id = id;
    players.push_back(player);
    game_name = player.name + "'s Game";
}

/**************************************************************************************************
 * Method:          Update()
 *
 * Purpose:         The main update loop for GameServer.  Handles the state switching and
 *                  delegating update requirements.
 *
 * Called By:       BeefServer, on a loop.
 *
 * Returns:         True the game has ended; false otherwise.
 *
 *************************************************************************************************/
GameState GameServer::Update()
{
    switch (state)
    {
    case GameState::Lobby:
        break;
    case GameState::Loading:
        loadingGame();
        break;
    case GameState::Game:
        if (updateGame())
        {
            state = GameState::Cleanup;
            unsigned disconnected_players = 0;
            for (auto& player : players)
            {
                if (player.status == PlayerStatus::Disconnected)
                {
                    player.game_id = -1;
                    ++disconnected_players;
                }
            }

            if (disconnected_players >= players.size())
            {
                state = GameState::PostGame;
            }
        }
        break;
    case GameState::Cleanup:
    case GameState::PostGame:
        break;
    }

    return state;
}

void GameServer::loadingGame()
{
    float duration = clock.getElapsedTime().asSeconds();

    for (PlayerData& player : players)
    {
        if (player.is_loaded)
        {
            bool found = false;
            for (auto& id : player_status)
            {
                if (id == player.player_id)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                cout << "Logging player [" << player.name << "] as loaded for game [" << id << "]" << endl;
                player_status.push_back(player.player_id);
            }
        }
    }

    if (player_status.size() == players.size())
    {
        startMatch();
        return;
    }

    if (duration > MATCH_START_TIMEOUT)
    {
        for (auto& player : players)
        {
            bool found = false;
            for (auto& status : player_status)
            {
                if (player.player_id == status)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                cout << "Player [" << player.name << "] was dropped because they timed out while loading." << endl;
                player.status = PlayerStatus::Disconnected;

                int remaining_players = 0;
                for (auto& player_data : players)
                {
                    if (player_data.status == PlayerStatus::Alive)
                    {
                        ++remaining_players;
                    }
                }

                player.rank = remaining_players;
                player.survival_time = 0;
            }
        }

        startMatch();
        return;
    }
}

void GameServer::startMatch()
{
    cout << "Informing all players that game [" << id << "] is starting." << endl;
    uint8_t code = static_cast<uint8_t>(START_GAME);

    int message_size = 1 + (8 * players.size());
    std::vector<sf::Vector2f> spawn_positions;
    spawn_positions.push_back({0, 300});
    spawn_positions.push_back({0, -300});
    spawn_positions.push_back({-200, 150});
    spawn_positions.push_back({-200, -150});
    spawn_positions.push_back({200, 150});
    spawn_positions.push_back({200, -150});
    std::random_shuffle(spawn_positions.begin(), spawn_positions.end());

    uint8_t message[message_size];
    message[0] = code;

    int index = 1;
    for (unsigned i = 0; i < players.size(); ++i)
    {
        std::memcpy(&message[index], &spawn_positions[i].x, 4);
        index += 4;
        std::memcpy(&message[index], &spawn_positions[i].y, 4);
        index += 4;

        players[i].position = { static_cast<float>(spawn_positions[i].x),
                                static_cast<float>(spawn_positions[i].y)};
    }

    for (PlayerData& player : players)
    {
        if (player.status != PlayerStatus::Disconnected)
        {
            if (!player.socket->WriteSync(&message, message_size))
            {
                cout << "Player [" << player.name << "] was dropped due to a socket error." << endl;
                player.status = PlayerStatus::Disconnected;
                updateRank(player);
            }
            else
            {
                player.health = PLAYER_STARTING_HP;
                player.in_lava = false;
                player.orientation = 0;
                player.velocity = { 0, 0 };
                player.status = PlayerStatus::Alive;
            }
        }
    }

    for (auto& player : players)
    {
        player.StartClock();
    }
    clock.restart();
    start_clock.restart();
    sendClock.restart();
    state = GameState::Game;
    hosting_player = -1;
}

/**************************************************************************************************
 * Method:          updateGame()
 *
 * Purpose:         The update loop for when GameServer is in a match. Handles all state
 *                  retransmitions, collision detection, the island radius, damage, and
 *                  calculates whether the game has ended or not.
 *
 * Called By:       Update(), when the game_state == Game
 *
 * Returns:         True the game has ended; false otherwise.
 *
 *************************************************************************************************/
bool GameServer::updateGame()
{
    float elapsed = clock.restart().asSeconds();

    float timeSlept = sendClock.getElapsedTime().asSeconds();
    if (timeSlept >= (1.f / SERVER_MIN_TICK_RATE))
    {
        OKToSend = true;
        sendClock.restart();

        if (island_radius > 0)
        {
            island_radius -= ISLAND_SHRINK_RATE * timeSlept;
        }
        else
        {
            island_radius = 0;
        }
    }

    // Check to see if we have a winner
    int remaining_players = 0;
    int winner = -1;
    for (auto& player_data : players)
    {
        if (player_data.status == PlayerStatus::Alive)
        {
            ++remaining_players;
            winner = player_data.player_id;
        }
    }

    if (winner > -1)
    {
        for (auto& player : players)
        {
            if (player.player_id == winner)
            {
                player.rank = remaining_players;
                player.survival_time = start_clock.getElapsedTime().asSeconds();

                if (remaining_players == 1)
                {
                    for (auto& player_data : players)
                    {
                        if (player_data.player_id == winner)
                        {
                            player_data.rank = 1;
                            Finalize();
                            return true;
                        }
                    }
                }
            }
        }
    }


    // Update Players
    updatePlayers(elapsed);

    // Update projectiles
    for (auto p_iterator = projectiles.begin(); p_iterator != projectiles.end(); ++p_iterator)
    {
        ProjectileData& projectile = *p_iterator;
        projectile.time_to_live -= elapsed;
        projectile.safety_timeout -= elapsed;

        if (projectile.time_to_live <= 0)
        {
            projectiles.erase(p_iterator--);
            continue;
        }

        projectile.position.x += projectile.velocity.x * elapsed;
        projectile.position.y += projectile.velocity.y * elapsed;
    }

    for (auto& player : players)
    {
        if (player.status == PlayerStatus::Alive)
        {
            // Check for lava damage
            if (sqrt(pow(player.position.x, 2) + pow(player.position.y, 2)) > island_radius)
            {
                // Was the player already in lava or is this new?
                if (player.in_lava)
                {
                    player.health -= LAVA_DAMAGE * elapsed;
                }

                player.in_lava = true;
            }
            else
            {
                player.in_lava = false;
            }

            // Check for collisions
            for (auto p_iterator = projectiles.begin(); p_iterator != projectiles.end(); ++p_iterator)
            {
                ProjectileData& projectile = *p_iterator;

                int collision_threshold = PLAYER_RADIUS + projectile.radius;

                // Projectiles have a brief moment on spawn when they can't hit anything
                if (projectile.safety_timeout <= 0)
                {
                    if (sqrt(pow(player.position.x - projectile.position.x, 2) +
                            pow(player.position.y - projectile.position.y, 2)) < collision_threshold)
                    {
                        switch (projectile.type)
                        {
                            case ProjectileTypes::Fireball:
                            {
                                player.health -= FIREBALL_DAMAGE;
                                player.velocity.x += projectile.velocity.x * FIREBALL_KNOCKBACK;
                                player.velocity.y += projectile.velocity.y * FIREBALL_KNOCKBACK;
                                break;
                            }
                            case ProjectileTypes::Iceball:
                            {
                                player.health -= ICEBALL_DAMAGE;
                                player.velocity.x += projectile.velocity.x * ICEBALL_KNOCKBACK;
                                player.velocity.y += projectile.velocity.y * ICEBALL_KNOCKBACK;
                                player.frozen = ICEBALL_FREEZE_DURATION;
                                break;
                            }
                        }

                        ++num_collisions;

                        //cout << "Broadcasting to all players that projectile [" << static_cast<int>(projectile.id);
                        //cout << "] collided with player [" << player.name << "]" << endl;
                        uint8_t message[10];
                        message[0] = static_cast<uint8_t>(COLLISION_NOTIFICATION);
                        std::memcpy(&message[1], &num_collisions, 4);
                        message[5] = projectile.id;
                        std::memcpy(&message[6], &player.player_id, 4);
                        for (auto& player_data : players)
                        {
                            if (player_data.status != PlayerStatus::Disconnected)
                            {
                                if (!player_data.socket->WriteSync(message, sizeof(message)))
                                {
                                    cout << "Player [" << player.name << "] was dropped due to a socket error." << endl;
                                    player_data.status = PlayerStatus::Disconnected;
                                    if (updateRank(player_data))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }

                        //erase the projectile AFTER we are done using it.
                        projectiles.erase(p_iterator--);
                    }
                }
            }

            // Check if player is dead
            if (player.health <= 0)
            {
                player.status = PlayerStatus::Dead;
                if (updateRank(player))
                {
                    return true;
                }
            }
        }
    }

    if (OKToSend)
    {
        // Broadcast new state to all players
        if (sendStateUpdate())
        {
            return true;
        }
        OKToSend = false;
    }

    return false;
}

// Returns true if the game is over
bool GameServer::updateRank(PlayerData& player)
{
    int remaining_players = 0;
    int winner = -1;
    for (auto& player_data : players)
    {
        if (player_data.status == PlayerStatus::Alive)
        {
            ++remaining_players;
            winner = player_data.player_id;
        }
    }

    player.rank = remaining_players + 1;
    player.survival_time = start_clock.getElapsedTime().asSeconds();

    if (remaining_players == 1)
    {
        for (auto& player_data : players)
        {
            if (player_data.player_id == winner)
            {
                player_data.rank = 1;
                Finalize();
                return true;
            }
        }
    }

    return false;
}

void GameServer::updatePlayers(float elapsed)
{
    for (auto& player : players)
    {
        if (player.frozen > 0)
        {
            player.frozen -= elapsed;
        }
        else
        {
            player.frozen = 0;
        }

        addFriction(player, elapsed);

        if (player.was_updated)
        {
            player.UpdateVelocity();
        }

        player.position.x += player.velocity.x * elapsed;
        player.position.y += player.velocity.y * elapsed;

        player.was_updated = false;
    }
}

void GameServer::addFriction(PlayerData& player, float elapsed)
{
    // Apply friction
    if (player.velocity.x > 0)
    {
        player.velocity.x -= FRICTION * elapsed;
        if (player.velocity.x < 0)
        {
            player.velocity.x = 0;
        }
    }
    else if (player.velocity.x < 0)
    {
        player.velocity.x += FRICTION * elapsed;
        if (player.velocity.x > 0)
        {
            player.velocity.x = 0;
        }
    }

    if (player.velocity.y > 0)
    {
        player.velocity.y -= FRICTION * elapsed;
        if (player.velocity.y < 0)
        {
            player.velocity.y = 0;
        }
    }
    else if (player.velocity.y < 0)
    {
        player.velocity.y += FRICTION * elapsed;
        if (player.velocity.y > 0)
        {
            player.velocity.y = 0;
        }
    }
}

bool GameServer::sendStateUpdate()
{
    // calculate message size
    int message_size = 5;

    // Position + Orientation + Velocity + Health + Status
    message_size += (8 + 4 + 8 + 4 + 1) * players.size();


    // Build message
    uint8_t message[message_size];
    message[0] = STATE_UPDATE;
    std::memcpy(&message[1], &island_radius, 4);

    int index = 5;
    for (auto& player : players)
    {
        std::memcpy(&message[index], &player.position.x, 4);
        index += 4;
        std::memcpy(&message[index], &player.position.y, 4);
        index += 4;
        std::memcpy(&message[index], &player.orientation, 4);
        index += 4;
        std::memcpy(&message[index], &player.velocity.x, 4);
        index += 4;
        std::memcpy(&message[index], &player.velocity.x, 4);
        index += 4;
        std::memcpy(&message[index], &player.health, 4);
        index += 4;
        uint8_t status = static_cast<uint8_t>(player.status);
        std::memcpy(&message[index], &status, 1);
        index += 1;
    }

    // Broadcast message
    // cout << "Broadcasting State Update Message to all players in game [" << id << "]" << endl;

    for (auto& player : players)
    {
        if (player.status != PlayerStatus::Disconnected)
        {
            if (!player.socket->WriteSync(message, message_size))
            {
                player.status = PlayerStatus::Disconnected;
                return updateRank(player);
            }
        }
    }

    return false;
}

/**************************************************************************************************
 * Method:          JoinGame()
 * Parameters:      PlayerData player
 *                      The player trying to join the game
 *
 * Purpose:         Add a player to the game lobby. Function fails if the game is not currently
 *                  in the lobby phase.
 *
 * Called By:       BeefServer, on the event of receiving a [JOIN_GAME] message and parsing it
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field and
 *                  the [playerID] field.  The [player] structure is populated with [player_id],
 *                  [name], and [socket].
 *
 * Returns:         True if the player was added to the game; false otherwise.
 *
 *************************************************************************************************/
void GameServer::JoinGame(PlayerData& player)
{
    if (state != GameState::Lobby)
    {
        cout << "Sending error message: [" << GetErrorString(Error::GameAlreadyStarted);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::GameAlreadyStarted);
        return;
    }

    if (players.size() >= MAX_PLAYERS)
    {
        cout << "Sending error message: [" << GetErrorString(Error::GameIsFull);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::GameIsFull);
        return;
    }
    else
    {
        // Confirmation message
        int message_size = 6; // in bytes
        for (auto& player_data : players)
        {
            message_size += 4; // player_id
            message_size += 2; // 2-byte string length header
            message_size += player_data.name.size();
        }

        // Write header: CommandCode, GameID
        uint8_t confirmation_message[message_size];
        confirmation_message[0] = static_cast<uint8_t>(JOIN_GAME);
        std::memcpy(&confirmation_message[1], &id, 4);
        uint8_t num_players = static_cast<uint8_t>(players.size());
        std::memcpy(&confirmation_message[5], &num_players, 1);

        // Append each player's ID and Name to the message
        int index = 6;
        for (auto& player_data : players)
        {
            int16_t size = player_data.name.size();
            std::memcpy(&confirmation_message[index], &size, 2);
            index += 2;
            std::memcpy(&confirmation_message[index], player_data.name.c_str(), size);
            index += size;
            std::memcpy(&confirmation_message[index], &player_data.player_id, 4);
            index += 4;
        }

        // Send the message
        cout << "Sending acknowledgement that player [" << player.name << "] has joined game [" << id << "]" << endl;
        if (!player.socket->WriteSync(confirmation_message, message_size))
        {
            return;
        }

        // Add player to the internal structure
        player.game_id = id;
        players.push_back(player);


        // Broadcast the new player has joined to all other players
        cout << "Broadcasting to all other players that player [" << player.name << "] has joined game [" << id << "]" << endl;
        for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
        {
            // Do not send to the new player
            if (iterator->player_id != player.player_id)
            {
                // CommandCode + PlayerID + NameSize + Name
                int new_player_message_size = 1 + 4 + 2 + player.name.size();
                uint8_t join_message[new_player_message_size];
                join_message[0] = static_cast<uint8_t>(PLAYER_JOIN_GAME);
                std::memcpy(&join_message[1], &player.player_id, 4);
                int16_t size = player.name.size();
                std::memcpy(&join_message[5], &size, 2);
                std::memcpy(&join_message[7], player.name.c_str(), size);

                if (!iterator->socket->WriteSync(join_message, new_player_message_size))
                {
                    players.erase(iterator--);
                }
            }
        }

        return;
    }
}

/**************************************************************************************************
 * Method:          ExitGame()
 * Parameters:      PlayerData player
 *                      The player leaving the game.
 *                  bool player_exists
 *                      Whether or not the socket inside player() is open
 *
 * Purpose:         Remove a player from the game lobby.
 *
 * Called By:       BeefServer, on the event of receiving a [LEAVE_GAME] message and
 *                  parsing it.
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field, the
 *                  [playerID], and the [gameID] field.
 *
 * Returns:         True if the player removed was not the host; false otherwise.
 *
 *************************************************************************************************/
bool GameServer::ExitGame(PlayerData& player, bool player_exists)
{
    if (state == GameState::Lobby)
    {
        int player_id = player.player_id;

        for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
        {
            if (player_id == iterator->player_id)
            {
                players.erase(iterator--);

                if (player_exists)
                {
                    cout << "Sending acknowledgement that player [" << player.name << "] has left game [" << id << "]" << endl;
                    char ack[5];
                    ack[0] = static_cast<uint8_t>(LEAVE_GAME);
                    std::memcpy(&ack[1], &id, 4);
                    if (!player.socket->WriteSync(ack, 5))
                    {
                        // Player has already been dropped; take no action.
                    }
                }

                if (player_id == hosting_player)
                {
                    for (auto& player_data : players)
                    {
                        if (player_data.player_id != hosting_player)
                        {
                            cout << "Sending error message: [" << GetErrorString(Error::HostLeft);
                            cout << "] to player: " << player.name << endl;
                            sendError(player_data, Error::HostLeft);
                            // if sendError fails we don't care because lobby is closing
                        }
                    }

                    cout << "The host of game [" << id << "] has left." << endl;
                    return false;
                }
                else
                {
                    char message[1 + 4];
                    message[0] = PLAYER_LEAVE_GAME;
                    std::memcpy(&message[1], &player.player_id, 4);

                    cout << "Broadcasting to all other players that player [" << player.name << "] has left game [" << id << "]" << endl;
                    std::vector<PlayerData*> players_to_drop;
                    for (auto& player_data : players)
                    {
                        if (player_data.player_id != player.player_id)
                        {
                            if (!player_data.socket->WriteSync(message, 5))
                            {
                                players_to_drop.push_back(&player_data);
                            }
                        }
                    }

                    // The only reason this doesn't ruin our iterators is we're about to return anyway
                    // This might ruin everything anyway
                    for (auto& p : players_to_drop)
                    {
                        ExitGame(*p, false);
                    }

                    return true;
                }
            }
        }

        cout << "Sending error message: [" << GetErrorString(Error::NotInGame);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::NotInGame);
        return true;
    }

    cout << "Sending error message: [" << GetErrorString(Error::GameStartedCannotLeave);
    cout << "] to player: " << player.name << endl;
    sendError(player, Error::GameStartedCannotLeave);
    return true;
}

/**************************************************************************************************
 * Method:          BeginGame()
 * Parameters:      PlayerData player
 *                      The player requesting to begin the game
 *
 * Purpose:         Begin the sequence for starting a game
 *
 * Called By:       BeefServer, on the event of receiving a [LOAD_GAME] message and
 *                  parsing it.
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field and the
 *                  [playerID] field.
 *
 *************************************************************************************************/
void GameServer::BeginGame(PlayerData& player)
{
    if (state != GameState::Lobby)
    {
        cout << "Sending error message: [" << GetErrorString(Error::GameStartedCannotStart);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::GameStartedCannotStart);
        return;
    }

    if (hosting_player != player.player_id)
    {
        cout << "Sending error message: [" << GetErrorString(Error::CannotStartNotHost);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::CannotStartNotHost);
        return;
    }

    std::vector<Color> colors;
    colors.push_back(Red);
    colors.push_back(Orange);
    colors.push_back(Teal);
    colors.push_back(Green);
    colors.push_back(Blue);
    colors.push_back(Pink);
    colors.push_back(Purple);
    std::random_shuffle(colors.begin(), colors.end());

    cout << "Broadcasting to players";
    for (auto& player_data : players)
    {
        cout << " [" << player_data.name << "] ";
    }
    cout << "that game [" << id << "] is starting." << endl;

    for (auto& player_data : players)
    {
        // MessageID + NumPlayers + PlayerInfo * NumPlayers + GameInfo
        int message_size = 1 + 1 + (4 + 3) * players.size() + 0;
        uint8_t message[message_size];
        message[0] = static_cast<uint8_t>(LOAD_GAME);
        message[1] = static_cast<uint8_t>(players.size());

        int color_index = 0;
        int message_index = 2;
        for (auto& other_player : players)
        {
            Color color = colors[color_index++];
            std::memcpy(&message[message_index], &other_player.player_id, 4);
            message_index += 4;
            message[message_index++] = color.red;
            message[message_index++] = color.green;
            message[message_index++] = color.blue;
        }

        if (!player_data.socket->WriteSync(message, message_size))
        {
            player_data.status = PlayerStatus::Disconnected;
            updateRank(player_data);
        }
    }

    state = GameState::Loading;

    clock.restart();
}

/**************************************************************************************************
 * Method:          Chat()
 * Parameters:      PlayerData player
 *                      The player sending a chat message
 *
 * Purpose:         Parse a chat message from a player and send it out to the other players
 *
 * Called By:       BeefServer, on the event of receiving a [CHAT] and parsing the [player_id] and
 *                  [game_id] fields.
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field, the
 *                  [player_id] field, and the [game_id] field.
 *
 *************************************************************************************************/
void GameServer::Chat(PlayerData& player)
{
    bool found = false;
    for (PlayerData& data : players)
    {
        if (data.player_id == player.player_id)
        {
            found = true;
        }
    }
    if (!found)
    {
        cout << "Sending error message: [" << GetErrorString(Error::CannotChatNotInGame);
        cout << "] to player: " << player.name << endl;
        sendError(player, Error::CannotChatNotInGame);
    }

    uint16_t message_size;
    if (!player.socket->ReadSync(&message_size, 2))
    {
        if (state == GameState::Game)
        {
            player.status = PlayerStatus::Disconnected;
            updateRank(player);
        }
        else
        {
            for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
            {
                if (iterator->player_id == player.player_id)
                {
                    players.erase(iterator--);
                    break;
                }
            }
        }
    }

    char message[message_size];
    if (!player.socket->ReadSync(message, message_size))
    {
        if (state == GameState::Game)
        {
            player.status = PlayerStatus::Disconnected;
            updateRank(player);
        }
        else
        {
            for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
            {
                if (iterator->player_id == player.player_id)
                {
                    players.erase(iterator--);
                    break;
                }
            }
        }
    }

    // Identifier + PlayerID + MessageSize + Message
    int broadcast_message_size = 1 + 4 + 2 + message_size;
    char broadcast_message[broadcast_message_size];
    broadcast_message[0] = static_cast<uint8_t>(CHAT);
    std::memcpy(&broadcast_message[1], &player.player_id, 4);
    std::memcpy(&broadcast_message[5], &message_size, 2);
    std::memcpy(&broadcast_message[7], message, message_size);

    cout << "Received chat message [" << std::string(message, message_size) << "] from player [" << player.name << "]" << endl;
    for (auto iterator = players.begin(); iterator != players.end(); ++iterator)
    {
        if (iterator->status != PlayerStatus::Disconnected)
        {
            if (!iterator->socket->WriteSync(broadcast_message, broadcast_message_size))
            {
                if (state == GameState::Game)
                {
                    iterator->status = PlayerStatus::Disconnected;
                    updateRank(*iterator);
                }
                else
                {
                    players.erase(iterator--);
                }
            }
        }
    }
}

/**************************************************************************************************
 * Method:          UpdateState()
 * Parameters:      PlayerData player
 *                      The player sending a state update
 *
 * Purpose:         Update the server's information on a player's state
 *
 * Called By:       BeefServer, on the event of receiving a [STATE_UPDATE] and parsing the [player_id]
 *                  and [game_id] fields.
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field, the
 *                  [player_id] field, and the [game_id] field.
 *
 *************************************************************************************************/
void GameServer::UpdateState(PlayerData& player)
{
    // cout << "Received player update for player [" << player.player_id << "]" << endl;
    int32_t sequence_number;

    bool w, a, s, d;

    if (!player.socket->ReadSync(&sequence_number, 4))
    {
        player.status = PlayerStatus::Disconnected;
        updateRank(player);
        return;
    }

    if (!player.socket->ReadSync(&w, 1))
    {
        player.status = PlayerStatus::Disconnected;
        updateRank(player);
        return;
    }

    if (!player.socket->ReadSync(&a, 1))
    {
        player.status = PlayerStatus::Disconnected;
        updateRank(player);
        return;
    }

    if (!player.socket->ReadSync(&s, 1))
    {
        player.status = PlayerStatus::Disconnected;
        updateRank(player);
        return;
    }

    if (!player.socket->ReadSync(&d, 1))
    {
        player.status = PlayerStatus::Disconnected;
        updateRank(player);
        return;
    }

    for (auto& player_data : players)
    {
        if (player_data == player)
        {
            if (sequence_number == num_collisions)
            {
                player_data.w = w;
                player_data.a = a;
                player_data.s = s;
                player_data.d = d;

                player_data.was_updated = true;
            }
        }
    }
}

void GameServer::CreateProjectile(ProjectileData& projectile, int player_id)
{
    uint8_t new_id = (projectiles.size())?projectiles[projectiles.size()-1].id + 1: 0;
    projectile.id = new_id;

    switch (projectile.type)
    {
        case ProjectileTypes::Fireball:
        {
            projectile.safety_timeout = FIREBALL_SAFETY;
            projectile.time_to_live = FIREBALL_LIFESPAN;
            projectile.radius = FIREBALL_RADIUS;
            break;
        }
        case ProjectileTypes::Iceball:
        {
            projectile.safety_timeout = ICEBALL_SAFETY;
            projectile.time_to_live = ICEBALL_LIFESPAN;
            projectile.radius = ICEBALL_RADIUS;
            break;
        }
        default:
        {
            cout << "Unknown projectile type requested." << endl;
        }
    }

    for (auto& player : players)
    {
        if (player.player_id == player_id)
        {
            projectile.position.x = player.position.x;
            projectile.position.y = player.position.y;
            break;
        }
    }

    projectiles.push_back(projectile);

    // Code + ID + Type + Position + Orientation + Velocity
    int message_size = 1 + 1 + 1 + 8 + 4 + 8;
    uint8_t message[message_size];

    message[0] = static_cast<uint8_t>(PROJ_NOTIFICATION);
    message[1] = static_cast<uint8_t>(projectile.id);
    message[2] = static_cast<uint8_t>(projectile.type);
    std::memcpy(&message[3], &projectile.position.x, 4);
    std::memcpy(&message[7], &projectile.position.y, 4);
    std::memcpy(&message[11], &projectile.orientation, 4);
    std::memcpy(&message[15], &projectile.velocity.x, 4);
    std::memcpy(&message[19], &projectile.velocity.y, 4);

    //cout << "Broadcasting to all players in game [" << id << "] that a new projectile was created." << endl;
    for (auto& player : players)
    {
        if (player.status != PlayerStatus::Disconnected)
        {
            if (!player.socket->WriteSync(message, message_size))
            {
                player.status = PlayerStatus::Disconnected;
                updateRank(player);
            }
        }
    }
}

void GameServer::FinalizeACK(int player_id)
{
    bool receivedAllACKs = true;
    for (auto& player : players)
    {
        if(player.player_id == player_id)
        {
            cout << "Finalize ACK received for player [" << player.player_id;
            cout << "] in game [" << player.game_id << "]" << endl;
            player.game_id = -1;
        }

        if(player.game_id != -1)
            receivedAllACKs = false;
    }

    if(receivedAllACKs)
        state = GameState::PostGame;
}

/**************************************************************************************************
 * Method:          NotifyLoaded()
 * Parameters:      int32_t player_id
 *                      The id of the player who sent the [IsLoaded] message.
 *
 * Purpose:         Notifies the game server that a player has loaded the game.
 *
 * Called By:       BeefServer, on the event of receiving a [START_GAME] and parsing the
 *                  [player_id] and [game_id] fields.
 *
 * Preconditions:   The socket object inside [player] has already read the [messageID] field, the
 *                  [player_id] field, and the [game_id] field.
 *
 *************************************************************************************************/
void GameServer::NotifyLoaded(int32_t player_id)
{
    cout << "Received Load Notification from player [" << player_id << "] in game [" << id << "]" << endl;
    for (auto& player : players)
    {
        if (player.player_id == player_id)
        {
            player.is_loaded = true;
        }
    }
}

void GameServer::Finalize()
{
    int message_size = 1 + players.size();

    uint8_t message[message_size];
    message[0] = static_cast<uint8_t>(GAME_FINISHED);

    int index = 1;
    for (auto& player : players)
    {
        std::memcpy(&message[index++], &player.rank, 1);
    }

    cout << "Broadcasting final rankings to all players for game [" << id << "]" << endl;
    for (auto& player : players)
    {
        if (player.status != PlayerStatus::Disconnected)
        {
            player.socket->WriteSync(message, message_size);
        }
        else
        {
            player.game_id = -1;
        }
    }
}

std::string GameServer::GetName() const
{
    return game_name;
}

int GameServer::GetHostID() const
{
    return hosting_player;
}

int GameServer::GetServerID() const
{
    return id;
}

std::vector<PlayerData> GameServer::GetPlayers() const
{
    return players;
}

GameState GameServer::GetState() const
{
    return state;
}

int GameServer::GetNumPlayers() const
{
    return players.size();
}


bool GameServer::sendError(PlayerData& player, Error e)
{
    if (player.status == PlayerStatus::Disconnected)
    {
        cout << "Cannot send error to a disconnected player." << endl;
        return false;
    }

    uint8_t message[3];
    message[0] = static_cast<uint8_t>(ERROR);
    uint16_t error = static_cast<uint16_t>(e);
    std::memcpy(&message[1], &error, 2);
    if (!player.socket->WriteSync(message, 3))
    {
        std::cerr << "Something went wrong writing to the socket in sendError" << std::endl;
        return false;
    }
    return true;
}
