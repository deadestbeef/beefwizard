#include "player_data.h"
#include <cmath>
#include <iostream>

#define PLAYER_MAX_HP 100
#define ACCELERATION 500 // pixels-per-second-per-second
#define MAX_PLAYER_VELOCITY 800

using std::cout;
using std::endl;

PlayerData::PlayerData(std::string name, std::shared_ptr<TCPSocket> socket, int player_id) :
    name{name}, socket{socket}, player_id{player_id}, game_id{-1}, status{PlayerStatus::Alive},
    is_loaded{false}, health{PLAYER_MAX_HP}, position{0, 0}, velocity{0, 0}, orientation{0},
    in_lava{false}, rank{-1}, survival_time{-1}, was_updated{false}, w{0}, a{0}, s{0}, d{0}, frozen{0}
{
}

void PlayerData::StartClock()
{
    clock.restart();
}

void PlayerData::UpdateVelocity()
{
    float elapsed = clock.restart().asSeconds();

    if (frozen > 0)
    {
        return;
    }

    float magnitude = GetVelocityMagnitude();
    float x_mag = 0;
    float y_mag = 0;

    if (d)
    {
        x_mag += ACCELERATION * elapsed;
    }

    if (a)
    {
        x_mag -= ACCELERATION * elapsed;
    }

    if (w)
    {
        y_mag -= ACCELERATION * elapsed;
    }

    if (s)
    {
        y_mag += ACCELERATION * elapsed;
    }

    if (magnitude < MAX_PLAYER_VELOCITY)
    {
        velocity.x += x_mag;
        velocity.y += y_mag;

        if ((velocity.x > 0 && x_mag < 0) || (velocity.x < 0 && x_mag > 0))
        {
            velocity.x += x_mag;
        }

        if ((velocity.y > 0 && y_mag < 0) || (velocity.y < 0 && y_mag > 0))
        {
            velocity.y += y_mag;
        }
    }
    else
    {
        bool x = false;
        bool y = false;
        if (sqrt(pow(velocity.x + x_mag, 2) + pow(velocity.y, 2)) < magnitude)
        {
            x = true;
        }

        if (sqrt(pow(velocity.x, 2) + pow(velocity.y + y_mag, 2)) < magnitude)
        {
            y = true;
        }

        if (x)
        {
            velocity.x += x_mag;
            if ((velocity.x > 0 && x_mag < 0) || (velocity.x < 0 && x_mag > 0))
            {
                velocity.x += x_mag;
            }
        }

        if (y)
        {
            velocity.y += y_mag;
            if ((velocity.y > 0 && y_mag < 0) || (velocity.y < 0 && y_mag > 0))
            {
                velocity.y += y_mag;
            }
        }
    }
}

float PlayerData::GetVelocityMagnitude()
{
    return sqrt(pow(velocity.x, 2) + pow(velocity.y, 2));
}

bool PlayerData::operator==(const PlayerData& other) const
{
    return player_id == other.player_id;
}

bool PlayerData::operator<(const PlayerData& other) const
{
    return player_id < other.player_id;
}

bool PlayerData::operator>(const PlayerData& other) const
{
    return player_id > other.player_id;
}

bool PlayerData::operator<=(const PlayerData& other) const
{
    return player_id <= other.player_id;
}

bool PlayerData::operator>=(const PlayerData& other) const
{
    return player_id >= other.player_id;
}
