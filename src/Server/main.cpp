#include <iostream>
#include "beef_server.h"

using std::cout;
using std::endl;

int main()
{
    BeefServer server;

    cout << "Server starting." << endl;

    while (true)
    {
        server.Update();
    }
}
