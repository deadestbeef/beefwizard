#include <string>
#include "error_codes.h"

std::string GetErrorString(Error code)
{
    switch (code)
    {
        case Error::NotLoggedIn:
            return "Not logged in.";
        case Error::NameNotAvailable:
            return "Name is currently in use.";
        case Error::AlreadyHosting:
            return "This player is already hosting a game.";
        case Error::AlreadyInGame:
            return "This player is already in a game.";
        case Error::GameNotFound:
            return "The specified game ID could not be found.";
        case Error::AlreadyLoaded:
            return "Game has already loaded.";
        case Error::GameAlreadyStarted:
            return "Cannot join games that have already started.";
        case Error::GameIsFull:
			return "Cannot join games that are full.";
        case Error::HostLeft:
			return "The host has left the game.";
        case Error::NotInGame:
            return "Cannot leave a game you are not in.";
        case Error::GameStartedCannotLeave:
            return "Cannot leave a game that has already started.";
        case Error::GameStartedCannotStart:
            return "Cannot start a game that has already started.";
        case Error::CannotStartNotHost:
            return "Cannot start game for which you are not the host.";
        case Error::CannotChatNotInGame:
            return "Cannot send chat messages to games you are not in.";
        case Error::CannotChatServerDoesNotExist:
            return "Cannot send chat messages to servers that do not exist.";
        case Error::CannotCreateProjectileGameDoesNotExist:
            return "Cannot create projectiles in games that do not exist.";
        default:
            return "WE DON'T HAVE THIS ERROR YET :(";
    }
}