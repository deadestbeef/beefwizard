#include "iceball.h"
#include "game_manager.h"
#include "game_constants.h"
#include <iostream>
#include <functional>
#include <cmath>

using std::cout;
using std::endl;

Iceball::Iceball(sf::Vector2f position, sf::Vector2f velocity, float rotation, uint8_t ID):
    id{ID}, time_to_live{ICEBALL_LIFESPAN}
{
    sprite.setTexture(GameManager::iceballTexture);
    auto size = GameManager::iceballTexture.getSize();
    sprite.setOrigin(sf::Vector2f(size.x / 2, size.y / 2));
    sprite.setPosition(position);
    sprite.setRotation(rotation);

    this->velocity = velocity;
}

bool Iceball::Update(sf::Time elapsed)
{
    time_to_live -= elapsed.asSeconds();
    if (time_to_live < 0)
    {
        return false;
    }
    sprite.move(velocity * elapsed.asSeconds());
    return true;
}

void Iceball::Draw(sf::RenderWindow& window)
{
    window.draw(sprite);
}

uint8_t Iceball::getID()
{
    return id;
}