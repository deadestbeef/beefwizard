#include "player.h"
#include "game_manager.h"
#include <iostream>
#include <cmath>

using std::cout;
using std::endl;

#define VELOCITY_PER_SEC 2
#define ACCELERATION_CONSTANT 350
#define MAX_PLAYER_VELOCITY 700 //pixels per second
#define FIREBALL_COOLDOWN 2 //seconds


Player::Player() :velocity{0, 0}, status{PlayerStatus::Alive}, frozen{false}, elapsed{5}
{
    auto baseSize = GameManager::playerBase.getSize();
    baseSprite.setTexture(GameManager::playerBase);
    baseSprite.setOrigin(baseSize.x/2, baseSize.y/2);

    shadingSprite.setTexture(GameManager::playerShading);
    shadingSprite.setOrigin(baseSize.x/2, baseSize.y/2);

    auto hatSize = GameManager::enemyHat.getSize();
    hatSprite.setTexture(GameManager::enemyHat);
    hatSprite.setOrigin(hatSize.x/2, hatSize.y/2);

    auto iceblockSize = GameManager::iceblock.getSize();
    iceblockSprite.setTexture(GameManager::iceblock);
    iceblockSprite.setOrigin(iceblockSize.x / 2, iceblockSize.y / 2);

    healthBar.setSize(sf::Vector2f(100,10));
    healthBar.setFillColor(sf::Color::Green);

    playerName.setFont(GameManager::globalFont);
    playerName.setCharacterSize(24);
}

void Player::setPlayerName(std::string name)
{
    playerName.setString(name);
    playerName.setOrigin(playerName.getLocalBounds().width/2, playerName.getLocalBounds().height/2);
}

void Player::setPlayerColor(sf::Color color)
{
    baseSprite.setColor(color);
    shadingSprite.setColor(color);
}

void Player::setPosition(sf::Vector2f position)
{
    baseSprite.setPosition(position);
    shadingSprite.setPosition(position);
    hatSprite.setPosition(position.x+10, position.y-70);
    healthBar.setPosition(position.x-50, position.y-30);
    playerName.setPosition(position);
    iceblockSprite.setPosition(position);
}

sf::Vector2f Player::getPosition()
{
    return baseSprite.getPosition();
}

void Player::clearHealth()
{
    healthBar.setSize(sf::Vector2f(0,0));
}

void Player::setAsPlayer()
{
    auto hatSize = GameManager::playerHat.getSize();
    hatSprite.setTexture(GameManager::playerHat);
    hatSprite.setOrigin(hatSize.x/2, hatSize.y/2);
}

PlayerStatus Player::getStatus()
{
    return status;
}

void Player::update(sf::Vector2f position, float orientation, sf::Vector2f velocity,
                float health, PlayerStatus status)
{
    elapsed += clock.restart().asSeconds();

    //orientation used later
    this->velocity = velocity;
    healthBar.setSize(sf::Vector2f(health,10));
    this->status = status;

    setPosition(position);
}

void Player::SetFrozen()
{
    elapsed = 0;
}

void Player::draw(sf::RenderWindow& window)
{
    window.draw(baseSprite);
    window.draw(shadingSprite);
    window.draw(hatSprite);
    window.draw(healthBar);
    window.draw(playerName);

    if (elapsed < ICEBALL_FREEZE_DURATION)
    {
        window.draw(iceblockSprite);
    }
}
