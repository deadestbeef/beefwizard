#include "lobby.h"
#include "game_manager.h"
#include "command_codes.h"
#include "error_codes.h"
#include <iostream>


Lobby::Lobby(sf::Vector2i ratio, TCPSocket& sock):aspect_ratio{ratio}, socket{sock},
                                 isHosting{false}, gameNameSet{false}, players{0},
                                 popup{ratio}, displayingPopup{false}
{
    m_font.loadFromFile("assets/Vera.ttf");

    gameNameText.setFont(m_font);
    gameNameText.setPosition(100, 10);

    for(int i = 0; i < MAX_PLAYERS; i++)
    {
        playerNameTexts[i].setFont(m_font);
        playerNameTexts[i].setCharacterSize(20);
    }

    for (int i = 0; i < MAX_CHAT_LINES; ++i)
    {
        chatRows[i].setFont(m_font);
        chatRows[i].setCharacterSize(18);
    }

    playerListTexture.loadFromFile("assets/PlayerListFrame.jpg");
    playerListSprite.setTexture(playerListTexture);

    playerChatBoxTexture.loadFromFile("assets/LobbyChatFrame.png");
    playerChatBoxSprite.setTexture(playerChatBoxTexture);
    playerChatBoxSprite.setOrigin(playerChatBoxTexture.getSize().x, 0);

    playerChatText.setFont(m_font);
    playerChatText.setCharacterSize(18);

    cursorTexture.loadFromFile("assets/text_cursor.png");
    cursorSprite.setTexture(cursorTexture);
    cursorSprite.setOrigin(0, cursorTexture.getSize().y/2);
    cursorSprite.setScale(0.6, 0.6);

    cursorShowing = true;

    startButtonTexture.loadFromFile("assets/StartButtonUp.jpg");
    startButtonTexture.setSmooth(true);
    startButtonSprite.setTexture(startButtonTexture);
    startButtonSprite.setOrigin(0, startButtonTexture.getSize().y);
    startButtonSprite.setScale(.94f, .94f);

    backButtonTexture.loadFromFile("assets/BackArrow.png");
    backButtonSprite.setTexture(backButtonTexture);
    backButtonSprite.setOrigin(backButtonTexture.getSize().x/2,backButtonTexture.getSize().y/2);
    backButtonSprite.setScale(.3f, .3f);
    backButtonSprite.setPosition(42,27);

    resize(ratio);
}

void Lobby::reset()
{
    players = 0;
    isHosting = false;
    gameNameSet = false;
    displayingPopup = false;

    for(int i = 0; i < MAX_PLAYERS; i++)
        playerNameTexts[i].setString("");

    chatBox.clear();

    for (int i = 0; i < MAX_CHAT_LINES; ++i)
        chatRows[i].setString("");
}

GameState Lobby::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    cursorCounter += elapsed;

    if(cursorCounter.asSeconds() > 1)
    {
        cursorShowing = !cursorShowing;
        cursorCounter = sf::Time::Zero;
    }

    if(!gameNameSet)
    {
        gameNameText.setString(GameManager::getGameName());
        gameNameSet = true;

        isHosting = GameManager::isHost();

        for(int i = 0; i < GameManager::numberOfPlayers; i++)
            playerNameTexts[i].setString(GameManager::playerNames[i]);
    }

    bool windowFocused = GameManager::isFocused;

    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);

    if(displayingPopup)
    {
        bool returnValue = false;

        if(windowFocused)
            displayingPopup = popup.update(mousePos, returnValue);

        if(returnValue)
        {
            if(leaveLobby)
            {
                if(isHosting)
                    exitLobby();
                return GameState::MENU_STATE;
            }
        }
    }
    else
    {
        if(windowFocused && backButtonSprite.getGlobalBounds().contains(mousePos))
        {
            backButtonSprite.setScale(.4f, .4f);

            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                if(isHosting)
                {
                    popup.show(Popup::YESNO, "You are hosting this game. Are you sure you want to leave?");
                    displayingPopup = true;
                    leaveLobby = true;
                    return GameState::LOBBY_STATE;
                }

                exitLobby();
                return GameState::MENU_STATE;
            }
        }
        else
        {
            backButtonSprite.setScale(.3f, .3f);
        }

        if( windowFocused && isHosting && startButtonSprite.getGlobalBounds().contains(mousePos))
        {
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                if(GameManager::numberOfPlayers < 2)
                {
                    popup.show(Popup::OK, "Can't start a game with one player.");
                    displayingPopup = true;
                    leaveLobby = false;
                    return GameState::LOBBY_STATE;
                }
                else
                {
                    //so that the host doesn't keep clicking the button on the next frames
                    isHosting = false;

                    beginInitializeGame();
                    return GameState::LOBBY_STATE;
                }
            }
        }
        else
        {
            //backButtonSprite.setScale(.3f, .3f);
        }

        if(windowFocused)
        {
            for(unsigned int i = 0; i < events.size(); i++)
            {
                if(events[i].type == sf::Event::TextEntered)
                {
                    // Strip any leading whitespace
                    size_t firstChar = 0;
                    while (firstChar < enteredText.getSize())
                    {
                        if (enteredText[firstChar] != ' ' && enteredText[firstChar] != '\t')
                            break;
                        else
                            ++firstChar;
                    }
                    enteredText = enteredText.substring(firstChar);

                    //enter key
                    if(events[i].text.unicode == 13)
                    {
                        // Don't send empty messages
                        if (enteredText.getSize())
                        {
                            sendChat();
                            enteredText.clear();
                        }
                    }
                    //backspace
                    else if(events[i].text.unicode == 8)
                    {
                        if (enteredText.getSize() > 0)
                            enteredText.erase(enteredText.getSize()-1);
                    }
                    else
                    {
                        // Allow reasonable characters only
                        sf::Uint32 val = events[i].text.unicode;
                        if (val > 31 && val < 128)
                        {
                            if (val != '\t')
                            {
                                sf::Text fake;
                                fake.setFont(m_font);
                                fake.setCharacterSize(18);
                                fake.setString(GameManager::getPlayerName() + enteredText);
                                if(fake.getGlobalBounds().width <= MAX_CHAT_WIDTH)
                                {
                                    enteredText += val;
                                }
                            }
                        }
                    }

                    playerChatText.setString(enteredText);
                    cursorShowing = true;
                }
            }
        }


        uint8_t code;
        if(socket.Read(&code, sizeof(code)))
        {
            switch(code)
            {
                case CommandCodes::ERROR:
                {
                    int16_t errorCode;
                    socket.ReadSync(&errorCode, sizeof(errorCode));
                    std::cout << GetErrorString(static_cast<Error>(errorCode)) << std::endl;

                    if(static_cast<Error>(errorCode) == Error::HostLeft)
                    {
                        popup.show(Popup::OK, GetErrorString(static_cast<Error>(errorCode)));
                        displayingPopup = true;
                        leaveLobby = true;
                    }

                    break;
                }
                case CommandCodes::PLAYER_JOIN_GAME:
                {
                    //[5][Player ID (int)][Player Name (str)]

                    int32_t playerID;
                    socket.ReadSync(&playerID, sizeof(playerID));

                    char buffer[50];
                    uint16_t stringSize;
                    socket.ReadSync(&stringSize, sizeof(stringSize));
                    socket.ReadSync(buffer, stringSize * sizeof(char));

                    GameManager::playerNames[GameManager::numberOfPlayers] = std::string(buffer, stringSize);
                    GameManager::playerIDs[GameManager::numberOfPlayers] = playerID;

                    GameManager::numberOfPlayers++;

                    for(int i = 0; i < GameManager::numberOfPlayers; i++)
                        playerNameTexts[i].setString(GameManager::playerNames[i]);

                    break;
                }
                case CommandCodes::PLAYER_LEAVE_GAME:
                {
                    int32_t playerID;
                    socket.ReadSync(&playerID, sizeof(playerID));

                    int index;
                    for(index = 0; index < GameManager::numberOfPlayers; index++)
                    {
                        if(playerID == GameManager::playerIDs[index])
                            break;
                    }

                    for(int i = index; i < GameManager::numberOfPlayers-1; i++)
                    {
                        GameManager::playerNames[i] = GameManager::playerNames[i+1];
                        GameManager::playerIDs[i] =  GameManager::playerIDs[i+1];
                    }
                    GameManager::numberOfPlayers--;

                    for(int i = 0; i < MAX_PLAYERS; i++)
                        playerNameTexts[i].setString("");

                    for(int i = 0; i < GameManager::numberOfPlayers; i++)
                        playerNameTexts[i].setString(GameManager::playerNames[i]);

                    break;
                }
                case CommandCodes::LOAD_GAME:
                {
                    window.setActive(true);
                    initializeGame();
                    return GameState::INGAME_STATE;
                }
                case CommandCodes::CHAT:
                {
                    int32_t pid;
                    socket.ReadSync(&pid, sizeof(pid));

                    std::string text = socket.ReadString();
                    if (chatBox.size() == MAX_CHAT_LINES)
                        chatBox.pop_back();

                    for (int i = 0; i < GameManager::numberOfPlayers; ++i)
                        if (GameManager::playerIDs[i] == pid)
                        {
                            text = GameManager::playerNames[i] + ":  " + text;
                            break;
                        }

                    chatBox.push_front(text);
                    break; // Don't do what Jeremy did...
                }
                default:
                {
                    std::cout << "Unknown command code " << std::to_string(static_cast<int>(code)) << std::endl;
                    break;
                }
            }
        }
    }

    return GameState::LOBBY_STATE;
}


void Lobby::draw(sf::RenderWindow& window)
{
    window.draw(gameNameText);
    window.draw(backButtonSprite);
    window.draw(playerListSprite);
    window.draw(playerChatBoxSprite);
    window.draw(gameNameText);

    for(int i = 0; i < MAX_PLAYERS; i++)
        window.draw(playerNameTexts[i]);

    if(isHosting)
        window.draw(startButtonSprite);

    window.draw(playerChatText);
    float textX = playerChatText.getLocalBounds().width + playerChatText.getPosition().x;
    cursorSprite.setPosition(textX + 2, playerChatText.getPosition().y + 11);

    for (size_t i = 0; i < chatBox.size(); ++i)
    {
        chatRows[i].setString(chatBox[i]);
        window.draw(chatRows[i]);
    }

    if(cursorShowing)
        window.draw(cursorSprite);

    if(displayingPopup)
        popup.draw(window);
}

void Lobby::resize(sf::Vector2i ratio)
{
    //resize layout of elements

    //auto bounds = gameNameText.getLocalBounds();
    for(int i = 0; i < MAX_PLAYERS; i++)
        playerNameTexts[i].setPosition(50, ratio.y/8+50+(i*50));

    playerListSprite.setPosition(20, ratio.y/8);

    float chatBoxPosition = (ratio.x <= 1400)?ratio.x-20:1400-20;

    playerChatBoxSprite.setPosition(chatBoxPosition, ratio.y/8);
    playerChatText.setPosition(playerChatBoxSprite.getPosition().x - 628, playerChatBoxSprite.getPosition().y + 444);
    for (int i = 0; i < MAX_CHAT_LINES; ++i)
    {
        chatRows[i].setPosition(playerChatText.getPosition().x - 50, playerChatText.getPosition().y - (25*(i+2)));
    }
    startButtonSprite.setPosition(20, ratio.y-40);

    popup.resize(ratio);
}

void Lobby::exit()
{
    if(gameNameSet)
    exitLobby();
}

void Lobby::exitLobby()
{
    //send game leave status
    //[7][Player ID (i)][ Game ID (i)]

    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = LEAVE_GAME;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    if(!socket.ReadSync(&code, sizeof(code)))
    {
        //if the server is down, who cares?
        return;
    }

    if(code == LEAVE_GAME)
    {
        int32_t ACK;
        socket.ReadSync(&ACK, sizeof(ACK));
        if(gameID == ACK)
        {
            //clear the game info for the game we left
            GameManager::setGameID(-1);
            GameManager::setGameName("");
            GameManager::setHosting(false);

            std::cout << "Managed to leave the game successfully" << std::endl;
            return;
        }
    }

    // Clearing game data is duplicated right now in case an error happens,
    // but errors will be handled at some later date
    GameManager::setGameID(-1);
    GameManager::setGameName("");
    GameManager::setHosting(false);

    std::cout << "Something happened while trying to leave a game" << std::endl;
}

void Lobby::beginInitializeGame()
{
    //[8][Player ID (int)][Game ID (int)]
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = LOAD_GAME;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));
}

void Lobby::initializeGame()
{
    //[8][Game Info][Number of Players (byte)][Player Info][...]

    TCPSocket& socket = GameManager::getTCPSocket();

    //game info is currently 0 bytes

    uint8_t playerCount;
    socket.ReadSync(&playerCount, sizeof(playerCount));

    //[Player ID (int)],[Player Color (byte,byte,byte)]

    for(int i = 0; i < playerCount; i++)
    {
        //for now, assume that the order on the server is the same order we already have
        //so we don't bother with the player names

        uint8_t color[3];
        int32_t playerID;

        socket.ReadSync(&playerID, sizeof(playerID));
        socket.ReadSync(color, sizeof(color));

        GameManager::playerIDs[i] = playerID;
        GameManager::playerColors[i] = sf::Color(color[0], color[1], color[2]);
    }

    GameManager::numberOfPlayers = playerCount;
}

void Lobby::sendChat()
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = CHAT;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    socket.WriteString(enteredText.toAnsiString());
}