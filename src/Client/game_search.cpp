#include "game_search.h"
#include "game_manager.h"
#include "command_codes.h"
#include "error_codes.h"
#include <iostream>


GameSearch::GameSearch(sf::Vector2i ratio):aspect_ratio{ratio},gameNameList{nullptr},
                                           gameIDList{nullptr}, gamesFound{false},
                                           popup{ratio}, displayingPopup{false}
{
    m_font.loadFromFile("assets/Vera.ttf");

    findGamesText.setFont(m_font);
    findGamesText.setString("Find Games");
    findGamesText.setPosition(100, 10);

    for(int i = 0; i < GAMES_PER_PAGE; i++)
    {
        listedGames[i].setFont(m_font);
        listedGames[i].setPosition(100,100+(50*i));

        gameCapacities[i].setFont(m_font);
        gameCapacities[i].setPosition(800, 100+(50*i));
    }

    backButtonTexture.loadFromFile("assets/BackArrow.png");
    backButtonSprite.setTexture(backButtonTexture);
    backButtonSprite.setOrigin(backButtonTexture.getSize().x/2,backButtonTexture.getSize().y/2);
    backButtonSprite.setScale(.3f, .3f);
    backButtonSprite.setPosition(42,27);

    resize(ratio);

}

void GameSearch::reset()
{
    gamesFound = false;

    for(int i = 0; i < GAMES_PER_PAGE; i++)
    {
        listedGames[i].setScale(1,1);
        listedGames[i].setString("");
        gameCapacities[i].setScale(1,1);
        gameCapacities[i].setString("");
    }

    backButtonSprite.setScale(.3f, .3f);


    if(gameIDList != nullptr)
    {
        delete[] gameIDList;
        gameIDList = nullptr;
    }

    if(gameNameList != nullptr)
    {
        delete[] gameNameList;
        gameNameList = nullptr;
    }

    displayingPopup = false;
}

GameState GameSearch::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    if(!gamesFound)
    {
        gameSearch();
        gamesFound = true;
    }

    bool windowFocused = GameManager::isFocused;
    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);

    if(displayingPopup)
    {
        bool returnValue = false;

        if(windowFocused)
            displayingPopup = popup.update(mousePos, returnValue);

        if(returnValue)
        {
            reset();
        }
    }
    else
    {
        for(int i = 0; i < GAMES_PER_PAGE; i++)
        {
            if(listedGames[i].getGlobalBounds().contains(mousePos))
            {
                listedGames[i].setScale(1.5f,1.5f);
                gameCapacities[i].setScale(1.5f,1.5f);
                if(windowFocused && sf::Mouse::isButtonPressed(sf::Mouse::Left))
                {
                    if(joinGame(gameIDList[i], gameNameList[i]))
                    {
                        return GameState::LOBBY_STATE;
                    }

                    return GameState::GAME_SEARCH_STATE;
                }
            }
            else
            {
                listedGames[i].setScale(1,1);
                gameCapacities[i].setScale(1,1);
            }
        }

        if(backButtonSprite.getGlobalBounds().contains(mousePos))
        {
            backButtonSprite.setScale(.4f, .4f);
            if(windowFocused && sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                return GameState::MENU_STATE;
            }
        }
        else
        {
            backButtonSprite.setScale(.3f, .3f);
        }
    }


    return GameState::GAME_SEARCH_STATE;
}


void GameSearch::draw(sf::RenderWindow& window)
{
    window.draw(findGamesText);
    window.draw(backButtonSprite);
    for(int i = 0; i < GAMES_PER_PAGE; i++)
    {
        window.draw(listedGames[i]);
        window.draw(gameCapacities[i]);
    }

    if(displayingPopup)
        popup.draw(window);
}

bool GameSearch::gameSearch()
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = LIST_GAMES;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));


    if(!socket.ReadSync(&code, sizeof(code)))
    {
        return false;
    }

    //[3][Number of Games (int)]([Game Name (str)][ Game ID (int)][Num Players (byte)])[…]
    if(code == LIST_GAMES)
    {
        socket.ReadSync(&numberOfGames, sizeof(numberOfGames));

        if(!numberOfGames)
            return false; //this should be something else, not a bool

        char buffer[50];
        gameNameList = new std::string[numberOfGames];
        gameIDList = new int32_t[numberOfGames];
        gameNumbers = std::vector<int>(numberOfGames);
        for(int i = 0; i < numberOfGames; i++)
        {
            uint16_t stringSize;
            socket.ReadSync(&stringSize, sizeof(stringSize));
            socket.ReadSync(buffer, stringSize * sizeof(char));

            int32_t gameID;
            socket.ReadSync(&gameID, sizeof(gameID));

            uint8_t num_players;
            socket.ReadSync(&num_players, sizeof(num_players));
            // TODO: Actually care about the number of players

            gameNameList[i] = std::string(buffer, stringSize);
            gameIDList[i] = gameID;
            gameNumbers[i] = num_players;
        }

        int min = (GAMES_PER_PAGE < numberOfGames)?GAMES_PER_PAGE:numberOfGames;
        for(int i = 0; i < min; i++)
        {
            listedGames[i].setString(gameNameList[i]);
            gameCapacities[i].setString("("+std::to_string(gameNumbers[i])+"/"+std::to_string(MAX_PLAYERS)+")");
        }

        return true;
    }

    uint16_t errorCode;
    socket.ReadSync(&errorCode, sizeof(errorCode));

    return false;

}

bool GameSearch::joinGame(int32_t gameID, std::string gameName)
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = JOIN_GAME;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    socket.WriteSync(&gameID, sizeof(gameID));

    if(!socket.ReadSync(&code, sizeof(code)))
    {
        return false;
    }

    //[4][Game ID(int)][Player Count(byte)][[Player Name(str)],[Player ID(int)]][...]
    if(code == JOIN_GAME)
    {
        int32_t ACK;
        socket.ReadSync(&ACK, sizeof(ACK));

        if(ACK == gameID)
        {

            GameManager::setGameID(gameID);
            GameManager::setHosting(false);
            GameManager::setGameName(gameName);

            uint8_t numberOfPlayers;
            socket.ReadSync(&numberOfPlayers, sizeof(numberOfPlayers));
            GameManager::numberOfPlayers = numberOfPlayers;

            char buffer[50];
            for(int i = 0; i < numberOfPlayers; i++)
            {
                uint16_t stringSize;
                socket.ReadSync(&stringSize, sizeof(stringSize));
                socket.ReadSync(buffer, stringSize * sizeof(char));

                int32_t playerID;
                socket.ReadSync(&playerID, sizeof(playerID));

                GameManager::playerNames[i] = std::string(buffer, stringSize);
                GameManager::playerIDs[i] = playerID;

            }

            GameManager::playerNames[numberOfPlayers] = GameManager::getPlayerName();
            GameManager::playerIDs[numberOfPlayers] = GameManager::getPlayerID();
            GameManager::numberOfPlayers = numberOfPlayers+1;

            return true;
        }
        else
        {
            //some unknown error happened
            //and we should attempt to leave the game we tried to join and the
            //one we had ACK'd
            return false;
        }
    }

    uint16_t errorCode;
    socket.ReadSync(&errorCode, sizeof(errorCode));

    popup.show(Popup::OK, GetErrorString(static_cast<Error>(errorCode)));
    displayingPopup = true;

    return false;
}

void GameSearch::resize(sf::Vector2i ratio)
{
    //resize layout of elements

    popup.resize(ratio);
}

void GameSearch::exit()
{
    if(gameIDList != nullptr)
    {
        delete[] gameIDList;
        gameIDList = nullptr;
    }

    if(gameNameList != nullptr)
    {
        delete[] gameNameList;
        gameNameList = nullptr;
    }

    //send logout message
}