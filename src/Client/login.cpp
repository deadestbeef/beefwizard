#include "login.h"
#include "game_manager.h"
#include "command_codes.h"
#include "error_codes.h"
#include <iostream>


Login::Login(sf::Vector2i ratio):popup{ratio}, displayingPopup{false}
{
    m_logoTexture.loadFromFile("assets/logo.png");
    m_logoSprite.setTexture(m_logoTexture);

    auto textureSize = m_logoTexture.getSize();
    m_logoSprite.setOrigin(sf::Vector2f(textureSize.x/2, 0));

    m_font.loadFromFile("assets/Vera.ttf");
    m_promptText.setFont(m_font);
    m_promptText.setString("Enter your Player Name");
    m_promptText.setOrigin(m_promptText.getLocalBounds().width, m_promptText.getLocalBounds().height/2 + 5);

    m_inputText.setFont(m_font);
    m_inputText.setOrigin(0, m_promptText.getLocalBounds().height/2 + 5);

    m_boxTexture.loadFromFile("assets/InputBox.png");
    m_boxSprite.setTexture(m_boxTexture);
    m_boxSprite.setOrigin(0, m_boxTexture.getSize().y/2);

    m_cursorTexture.loadFromFile("assets/text_cursor.png");
    m_cursorSprite.setTexture(m_cursorTexture);
    m_cursorSprite.setOrigin(0, m_cursorTexture.getSize().y/2);

    m_cursorShowing = true;

    resize(ratio);
}

void Login::reset()
{
    //nothing yet
}

GameState Login::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    m_cursorCounter += elapsed;

    if(m_cursorCounter.asSeconds() > 1)
    {
        m_cursorShowing = !m_cursorShowing;
        m_cursorCounter = sf::Time::Zero;
    }

    bool windowFocused = GameManager::isFocused;

    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);

    if(displayingPopup)
    {
        bool returnValue = false;

        if(windowFocused)
            displayingPopup = popup.update(mousePos, returnValue);

        if(returnValue)
        {
            return GameState::LOGIN_STATE;
        }
    }
    else
    {
        if(windowFocused)
        {
            for(unsigned int i = 0; i < events.size(); i++)
            {
                if(events[i].type == sf::Event::TextEntered)
                {
                    // Strip any leading whitespace
                    size_t firstChar = 0;
                    while (firstChar < m_text.getSize())
                    {
                        if (m_text[firstChar] != ' ' && m_text[firstChar] != '\t')
                            break;
                        else
                            ++firstChar;
                    }
                    m_text = m_text.substring(firstChar);

                    //enter key
                    if(events[i].text.unicode == 13)
                    {
                        //send name to the server and wait for a response
                        if(registerPlayer(m_text.toAnsiString()))
                            return GameState::MENU_STATE;
                        else
                            return GameState::LOGIN_STATE;
                    }
                    //backspace
                    else if(events[i].text.unicode == 8)
                    {
                        if (m_text.getSize() > 0)
                            m_text.erase(m_text.getSize()-1);
                    }
                    else
                    {
                        // Only allow reasonable characters
                        sf::Uint32 val = events[i].text.unicode;
                        if (val > 31 && val < 128)
                        {
                            if (val != '\t')
                            {
                                // Limit name by the size it consumes on screen
                                sf::Text fake;
                                fake.setFont(m_font);
                                fake.setCharacterSize(m_inputText.getCharacterSize());
                                fake.setString(m_text + val);
                                if(fake.getGlobalBounds().width <= MAX_NAME_WIDTH)
                                    m_text += val;
                            }
                        }
                    }

                    m_inputText.setString(m_text);
                    float textX = m_screenWidth/2+16+m_inputText.getLocalBounds().width;
                    m_cursorSprite.setPosition(textX, m_cursorSprite.getPosition().y);
                    m_cursorShowing = true;
                }
            }
        }
    }

    return GameState::LOGIN_STATE;
}

void Login::draw(sf::RenderWindow& window)
{
    window.draw(m_logoSprite);
    window.draw(m_promptText);
    window.draw(m_boxSprite);
    window.draw(m_inputText);

    if(m_cursorShowing)
        window.draw(m_cursorSprite);

    if(displayingPopup)
        popup.draw(window);
}

void Login::resize(sf::Vector2i ratio)
{
    m_screenWidth = ratio.x;
    m_logoSprite.setPosition(ratio.x/2, 0);

    auto textureSize = m_logoTexture.getSize();
    m_promptText.setPosition(ratio.x/2 - 5, textureSize.y);
    m_inputText.setPosition(ratio.x/2+15, textureSize.y);
    m_boxSprite.setPosition(ratio.x/2+5, textureSize.y);
    m_cursorSprite.setPosition(ratio.x/2+16, textureSize.y);

    popup.resize(ratio);
}

bool Login::registerPlayer(std::string playerName)
{
    TCPSocket& socket = GameManager::getTCPSocket();
    uint8_t code = REGISTER;
    socket.WriteSync(&code, sizeof(code));

    uint16_t name_size = playerName.length();
    socket.WriteSync(&name_size, sizeof(name_size));

    socket.WriteSync(&playerName[0], name_size*sizeof(char));

    socket.ReadSync(&code, sizeof(code));//need to check if server crashes

    if(code != REGISTER)
    {
        std::cout << "Cound't register!" << std::endl;

        uint16_t errorCode;
        socket.ReadSync(&errorCode, sizeof(errorCode));

        popup.show(Popup::OK, GetErrorString(static_cast<Error>(errorCode)));
        displayingPopup = true;

        return false;
    }

    int32_t playerID;
    socket.ReadSync(&playerID, sizeof(playerID));

    GameManager::setPlayerName(playerName);
    GameManager::setPlayerID(playerID);

    return true;
}

void Login::exit()
{
    //nothing to do here
}