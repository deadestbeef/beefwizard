#include "game_manager.h"
#include "leaderboard.h"
#include "command_codes.h"
#include <iostream>


Leaderboard::Leaderboard(sf::Vector2i ratio):aspect_ratio{ratio}, scoresFound(false)
{
    m_font.loadFromFile("assets/Vera.ttf");

    leaderboardText.setFont(m_font);
    leaderboardText.setString("Leaderboards");
    leaderboardText.setPosition(100, 10);

    int cols[4] = {25, 400, 600, 800};
    for (int i = 0; i < COLUMNS; ++i)
    {
        header[i].setFont(m_font);
        header[i].setPosition(cols[i], 50);
    }

    header[0].setString("NAME");
    header[1].setString("GAMES\nWON");
    header[2].setString("GAMES\nPLAYED");
    header[3].setString("AVERAGE\nPLACEMENT");

    for(int i = 0; i < ENTRIES_PER_PAGE; i++)
    {
        for (int j = 0; j < COLUMNS; ++j)
        {
            listedScores[i][j].setFont(m_font);
            listedScores[i][j].setPosition(cols[j],150+(50*i));
        }
    }

    backButtonTexture.loadFromFile("assets/BackArrow.png");
    backButtonSprite.setTexture(backButtonTexture);
    backButtonSprite.setOrigin(backButtonTexture.getSize().x/2,backButtonTexture.getSize().y/2);
    backButtonSprite.setScale(.3f, .3f);
    backButtonSprite.setPosition(42,27);
}

void Leaderboard::reset()
{
    for(int i = 0; i < ENTRIES_PER_PAGE; i++)
    {
        for (int j = 0; j < COLUMNS; ++j)
        {
            listedScores[i][j].setScale(1,1);
            listedScores[i][j].setString("");
        }
    }

    backButtonSprite.setScale(.3f, .3f);


    if (playerScoreList.size())
        playerScoreList.clear();

    scoresFound = false;
}

GameState Leaderboard::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    if(!scoresFound)
    {
        scoresFound = getScores();
    }

    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);

    if(backButtonSprite.getGlobalBounds().contains(mousePos))
    {
        backButtonSprite.setScale(.4f, .4f);
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            return GameState::MENU_STATE;
        }
    }
    else
    {
        backButtonSprite.setScale(.3f, .3f);
    }


    return GameState::LEADERBOARD_STATE;
}


void Leaderboard::draw(sf::RenderWindow& window)
{
    window.draw(leaderboardText);
    window.draw(backButtonSprite);
    for (int i = 0; i < COLUMNS; ++i)
        window.draw(header[i]);
    for(int i = 0; i < ENTRIES_PER_PAGE; i++)
        for (int j = 0; j < COLUMNS; ++j)
            window.draw(listedScores[i][j]);
}

bool Leaderboard::getScores()
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = LEADERBOARD;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    if(!socket.ReadSync(&code, sizeof(code)))
    {
        return false;
    }

    if(code == LEADERBOARD)
    {
        socket.ReadSync(&numberOfScores, sizeof(numberOfScores));

        if(!numberOfScores)
            return true;

        playerScoreList.resize(numberOfScores);
        for(int i = 0; i < numberOfScores; i++)
        {
            // TODO: Care about return values for socket reads?
            playerScoreList[i].Name = socket.ReadString();
            socket.ReadSync(&playerScoreList[i].NumberOfWins, sizeof(playerScoreList[i].NumberOfWins));
            socket.ReadSync(&playerScoreList[i].GamesPlayed, sizeof(playerScoreList[i].GamesPlayed));
            socket.ReadSync(&playerScoreList[i].AveragePlacement, sizeof(playerScoreList[i].AveragePlacement));
        }

        int min = (ENTRIES_PER_PAGE < numberOfScores) ? ENTRIES_PER_PAGE : numberOfScores;
        for(int i = 0; i < min; i++)
        {
            listedScores[i][0].setString(playerScoreList[i].Name);
            listedScores[i][1].setString(std::to_string(playerScoreList[i].NumberOfWins));
            listedScores[i][2].setString(std::to_string(playerScoreList[i].GamesPlayed));
            listedScores[i][3].setString(std::to_string(playerScoreList[i].AveragePlacement));
        }

        return true;
    }

    uint16_t errorCode;
    socket.ReadSync(&errorCode, sizeof(errorCode));

    return false;
}

void Leaderboard::resize(sf::Vector2i ratio)
{
    //resize layout of elements
}

void Leaderboard::exit()
{

}