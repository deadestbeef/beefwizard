#include <popup.h>


Popup::Popup(sf::Vector2i ratio):type{OK}
{

    m_font.loadFromFile("assets/Vera.ttf");

    displayedText.setFont(m_font);
    displayedText.setCharacterSize(14);
    OKText.setFont(m_font);
    OKText.setString("OK");
    OKText.setOrigin(OKText.getLocalBounds().width/2, OKText.getLocalBounds().height/2);
    YESText.setFont(m_font);
    YESText.setString("YES");
    YESText.setOrigin(YESText.getLocalBounds().width/2, YESText.getLocalBounds().height/2);
    NOText.setFont(m_font);
    NOText.setString("NO");
    NOText.setOrigin(NOText.getLocalBounds().width/2, NOText.getLocalBounds().height/2);

    backgroundTexture.loadFromFile("assets/PopupBackground.png");
    backgroundSprite.setTexture(backgroundTexture);

    auto backgroundSize = backgroundTexture.getSize();
    backgroundSprite.setOrigin(backgroundSize.x/2, backgroundSize.y/2);
}

void Popup::show(PopupType displayType, std::string message)
{
    type = displayType;
    displayedText.setString(message);
    displayedText.setOrigin(displayedText.getLocalBounds().width/2, displayedText.getLocalBounds().height/2);
}

void Popup::resize(sf::Vector2i ratio)
{
    sf::Vector2f center = sf::Vector2f(ratio.x/2, ratio.y/2);
    backgroundSprite.setPosition(center);

    displayedText.setPosition(center.x, center.y-PopupWindowSize.y/4);
    OKText.setPosition(center.x, center.y+PopupWindowSize.y/4);
    YESText.setPosition(center.x - PopupWindowSize.x/4, center.y+PopupWindowSize.y/4);
    NOText.setPosition(center.x + PopupWindowSize.x/4, center.y+PopupWindowSize.y/4);
}

bool Popup::update(sf::Vector2f mousePos, bool& returnValue)
{
    if(type == OK)
    {
        if(OKText.getGlobalBounds().contains(mousePos))
        {
            OKText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                returnValue = true;
                return false;
            }
        }
        else
        {
            OKText.setScale(1,1);
        }
    }
    else
    {
        if(YESText.getGlobalBounds().contains(mousePos))
        {
            YESText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                returnValue = true;
                return false;
            }
        }
        else
        {
            YESText.setScale(1,1);
        }

        if(NOText.getGlobalBounds().contains(mousePos))
        {
            NOText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                returnValue = false;
                return false;
            }
        }
        else
        {
            NOText.setScale(1,1);
        }
    }

    return true;
}

void Popup::draw(sf::RenderWindow& window)
{
    window.draw(backgroundSprite);

    window.draw(displayedText);
    if(type == OK)
        window.draw(OKText);
    else
    {
        window.draw(YESText);
        window.draw(NOText);
    }
}
