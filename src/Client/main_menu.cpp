#include "main_menu.h"
#include "game_manager.h"
#include "command_codes.h"
#include <iostream>


MainMenu::MainMenu(sf::Vector2i ratio):aspect_ratio{ratio}, playerNameSet{false}
{
    m_font.loadFromFile("assets/Vera.ttf");

    playerNameText.setFont(m_font);
    playerNameText.setOrigin(playerNameText.getLocalBounds().width, 0);

    findGameText.setFont(m_font);
    findGameText.setString("Find Game");
    findGameText.setOrigin(0, findGameText.getLocalBounds().height/2+5);

    createGameText.setFont(m_font);
    createGameText.setString("Create Game");
    createGameText.setOrigin(0,createGameText.getLocalBounds().height/2+5);

    leaderboardText.setFont(m_font);
    leaderboardText.setString("Leaderboard");
    leaderboardText.setOrigin(0, leaderboardText.getLocalBounds().height/2+5);

    howToPlayText.setFont(m_font);
    howToPlayText.setString("How To Play");
    howToPlayText.setOrigin(0, howToPlayText.getLocalBounds().height/2+5);

    exitText.setFont(m_font);
    exitText.setString("Exit Game");
    exitText.setOrigin(0, exitText.getLocalBounds().height/2+5);

    resize(ratio);

}

void MainMenu::reset()
{
    findGameText.setScale(1,1);
    createGameText.setScale(1,1);
    leaderboardText.setScale(1,1);
    howToPlayText.setScale(1,1);
}

GameState MainMenu::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    if(!playerNameSet)
    {
        playerNameText.setString(GameManager::getPlayerName());
        playerNameSet = true;
    }

    if(GameManager::isFocused)
    {
        sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x,
                                             sf::Mouse::getPosition(window).y);

        if(findGameText.getGlobalBounds().contains(mousePos))
        {
            findGameText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                return GameState::GAME_SEARCH_STATE;
            }
        }
        else
            findGameText.setScale(1,1);

        if(createGameText.getGlobalBounds().contains(mousePos))
        {
            createGameText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                if(createGame())
                    return GameState::LOBBY_STATE;
                return GameState::MENU_STATE;
            }
        }
        else
            createGameText.setScale(1,1);

        if(leaderboardText.getGlobalBounds().contains(mousePos))
        {
            leaderboardText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                return GameState::LEADERBOARD_STATE;
            }
        }
        else
            leaderboardText.setScale(1,1);

        if(howToPlayText.getGlobalBounds().contains(mousePos))
        {
            howToPlayText.setScale(1.5f,1.5f);
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                return GameState::TUTORIAL_STATE;
            }
        }
        else
            howToPlayText.setScale(1,1);

        if(exitText.getGlobalBounds().contains(mousePos))
        {
            exitText.setScale(1.5f,1.5f);

            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                //inform the server that we're exiting
                window.close();
            }
        }
        else
            exitText.setScale(1,1);

    }

    return GameState::MENU_STATE;
}


void MainMenu::draw(sf::RenderWindow& window)
{
    window.draw(playerNameText);
    window.draw(findGameText);
    window.draw(createGameText);
    window.draw(leaderboardText);
    window.draw(howToPlayText);
    window.draw(exitText);
}

bool MainMenu::createGame()
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = CREATE_GAME;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));


    if(!socket.ReadSync(&code, sizeof(code)))
    {
        return false;
    }

    if(code == CREATE_GAME)
    {
        int32_t gameID;
        socket.ReadSync(&gameID, sizeof(gameID));
        GameManager::setGameID(gameID);
        GameManager::setGameName(GameManager::getPlayerName()+"\'s Game");
        GameManager::setHosting(true);

        GameManager::numberOfPlayers = 1;
        GameManager::playerIDs[0] = GameManager::getPlayerID();
        GameManager::playerNames[0] = GameManager::getPlayerName();

        return true;
    }

    uint16_t errorCode;
    socket.ReadSync(&errorCode, sizeof(errorCode));

    return false;

}

void MainMenu::resize(sf::Vector2i ratio)
{
    //resize layout of elements
    playerNameText.setPosition(ratio.x - ratio.x/3, 0);
    findGameText.setPosition(50, ratio.y/4);
    createGameText.setPosition(50, ratio.y/4 + ratio.y/6);
    leaderboardText.setPosition(50, ratio.y/4 + ratio.y/6 *2);
    howToPlayText.setPosition(50, ratio.y/4 + ratio.y/6 *3);
    exitText.setPosition(50, ratio.y/4 + ratio.y/6 *4);
}

void MainMenu::exit()
{
    //send logout message
}