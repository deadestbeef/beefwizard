#include "tutorial_scene.h"
#include "game_manager.h"

#include <iostream>

TutorialScene::TutorialScene(sf::Vector2i ratio):aspect_ratio{ratio}
{
    howToPlayText.setFont(GameManager::globalFont);
    howToPlayText.setPosition(100, 10);
    howToPlayText.setString("How To Play");

    backButtonTexture.loadFromFile("assets/BackArrow.png");
    backButtonSprite.setTexture(backButtonTexture);
    backButtonSprite.setOrigin(backButtonTexture.getSize().x/2,backButtonTexture.getSize().y/2);
    backButtonSprite.setScale(.3f, .3f);
    backButtonSprite.setPosition(42,27);

    movementText.setFont(GameManager::globalFont);
    movementText.setString("Use WASD to move your wizard. Your wizard wears a blue hat.");
    movementText.setCharacterSize(20);
    movementText.setOrigin(movementText.getLocalBounds().width/2, movementText.getLocalBounds().height/2+5);
    fireballText.setFont(GameManager::globalFont);
    fireballText.setString("Left click to launch a fireball. Fireballs move fast and knock enemy wizards back.");
    fireballText.setCharacterSize(20);
    fireballText.setOrigin(fireballText.getLocalBounds().width/2, fireballText.getLocalBounds().height/2+5);
    iceballText.setFont(GameManager::globalFont);
    iceballText.setString("Right click to launch an iceball. Iceballs move slow, but will freeze your enemies for a short time.");
    iceballText.setCharacterSize(20);
    iceballText.setOrigin(iceballText.getLocalBounds().width/2, iceballText.getLocalBounds().height/2+5);

    wasdTexture.loadFromFile("assets/wasd.png");
    wasdTexture.setSmooth(true);
    wasdSprite.setTexture(wasdTexture);
    wasdSprite.setOrigin(wasdTexture.getSize().x/2, wasdTexture.getSize().y/2);
    wasdSprite.setScale(.7f, .7f);
    mouseTexture.loadFromFile("assets/mouseclick.png");
    auto mouseTextureSize = mouseTexture.getSize();
    mouseTexture.setSmooth(true);
    leftClickSprite.setTexture(mouseTexture);
    rightClickSprite.setTexture(mouseTexture);
    leftClickSprite.setOrigin(mouseTextureSize.x/2, mouseTextureSize.y/2);
    rightClickSprite.setOrigin(mouseTextureSize.x/2, mouseTextureSize.y/2);
    leftClickSprite.setScale(.5f, .5f);
    rightClickSprite.setScale(-.5f, .5f);//negative scale flips the texture

    movementSpriteSheet.loadFromFile("assets/movement_tutorial.png");
    fireballSpriteSheet.loadFromFile("assets/fireball_tutorial.png");
    iceballSpriteSheet.loadFromFile("assets/iceball_tutorial.png");

    auto frameSize = sf::Vector2i(300, 150);

    movementAnimation = SpriteAnimation(movementSpriteSheet, frameSize, sf::Vector2i(4,7), 0.2f);
    fireballAnimation = SpriteAnimation(fireballSpriteSheet, frameSize, sf::Vector2i(3,6), 0.2f);;
    iceballAnimation = SpriteAnimation(iceballSpriteSheet, frameSize, sf::Vector2i(5,6), 0.1f);;

    resize(ratio);
}

void TutorialScene::reset()
{
    //nothing yet
}

GameState TutorialScene::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    bool windowFocused = GameManager::isFocused;

    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x,
                                         sf::Mouse::getPosition(window).y);

    if(windowFocused && backButtonSprite.getGlobalBounds().contains(mousePos))
    {
        backButtonSprite.setScale(.4f, .4f);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            return GameState::MENU_STATE;
        }
    }
    else
    {
        backButtonSprite.setScale(.3f, .3f);
    }

    movementAnimation.update(elapsed.asSeconds());
    fireballAnimation.update(elapsed.asSeconds());
    iceballAnimation.update(elapsed.asSeconds());

    return GameState::TUTORIAL_STATE;
}

void TutorialScene::draw(sf::RenderWindow& window)
{
    window.draw(howToPlayText);
    window.draw(backButtonSprite);
    window.draw(wasdSprite);
    window.draw(leftClickSprite);
    window.draw(rightClickSprite);
    window.draw(movementText);
    window.draw(fireballText);
    window.draw(iceballText);
    movementAnimation.draw(window);
    fireballAnimation.draw(window);
    iceballAnimation.draw(window);
}

void TutorialScene::resize(sf::Vector2i ratio)
{
    movementText.setPosition(ratio.x/2, ratio.y/4 - 100);
    movementAnimation.setPosition(sf::Vector2f(ratio.x/2 + 200, ratio.y/4));
    wasdSprite.setPosition(ratio.x/2 -200, ratio.y/4);

    float yCenter = ((ratio.y - 100) - (ratio.y/4))/2;

    fireballText.setPosition(ratio.x/2, yCenter + ratio.y/4 - 100);
    fireballAnimation.setPosition(sf::Vector2f(ratio.x/2 + 200, yCenter + ratio.y/4));
    leftClickSprite.setPosition(ratio.x/2 - 200, yCenter + ratio.y/4);

    iceballText.setPosition(ratio.x/2, ratio.y - 200);
    iceballAnimation.setPosition(sf::Vector2f(ratio.x/2 + 200, ratio.y - 100));
    rightClickSprite.setPosition(ratio.x/2 - 200, ratio.y - 100);
}


void TutorialScene::exit()
{
    //nothing to do here
}
