#include "sprite_animation.h"


SpriteAnimation::SpriteAnimation(sf::Texture& texture, sf::Vector2i frameSize, sf::Vector2i frameGrid, float timeBetweenFrames):
                                m_frameSize{frameSize}, m_frameGrid{frameGrid}, m_timeBetweenFrames{timeBetweenFrames}, currentTime{0},
                                currentFrame{0,0}
{
    m_sprite.setTexture(texture);

    m_sprite.setTextureRect(sf::IntRect(0,0,m_frameSize.x, m_frameSize.y));
    m_sprite.setOrigin(m_frameSize.x/2, m_frameSize.y/2);
}

void SpriteAnimation::setPosition(sf::Vector2f position)
{
    m_sprite.setPosition(position);
}

void SpriteAnimation::update(float elapsedTime)
{
    currentTime+= elapsedTime;

    if(currentTime >= m_timeBetweenFrames)
    {
        currentFrame.x+=1;
        if(currentFrame.x >= m_frameGrid.x)
        {
            currentFrame.x = 0;
            currentFrame.y+=1;
            if(currentFrame.y >= m_frameGrid.y)
                currentFrame.y = 0;
        }

        currentTime -= m_timeBetweenFrames;
    }

    m_sprite.setTextureRect(sf::IntRect(currentFrame.x * m_frameSize.x, currentFrame.y * m_frameSize.y,
                                        m_frameSize.x, m_frameSize.y));

}

void SpriteAnimation::draw(sf::RenderWindow& window)
{
    window.draw(m_sprite);
}