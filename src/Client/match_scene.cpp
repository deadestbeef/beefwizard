#include "match_scene.h"
#include "game_manager.h"
#include "command_codes.h"
#include "error_codes.h"
#include "game_constants.h"
#include <iostream>
#include <cstring>
#define _USE_MATH_DEFINES
#include <cmath>

#define VELOCITY_PER_SEC 2
#define ACCELERATION_CONSTANT 350
#define MAX_PLAYER_VELOCITY 700 //pixels per second
#define MAX_SEND_RATE 50 // Hertz

#define FIREBALL_COOLDOWN 2 //seconds
#define FIREBALL_VELOCITY 1000 // pixels per second

#define ICEBALL_COOLDOWN 4
#define ICEBALL_VELOCITY 500

MatchScene::MatchScene(sf::Vector2i ratio, TCPSocket& sock) : aspect_ratio{ratio}, original_ratio{ratio}, arenaLoaded{false},
                            socket{sock}, activeChat{false}, lastElapsedTime{0}, okayToSend{false}, sequenceNumber{0}
{
    m_font.loadFromFile("assets/Vera.ttf");
    playerChatText.setFont(m_font);
    playerChatText.setCharacterSize(18);
    playerChatText.setPosition(10, 18 + (20*MAX_CHAT_LINES));
    for (int i = 0; i < MAX_CHAT_LINES; ++i)
    {
        ingameChat[i].setFont(m_font);
        ingameChat[i].setCharacterSize(18);
        ingameChat[i].setPosition(10, 10 + (20*MAX_CHAT_LINES) - (20*(i+1)));
    }

    cursorTexture.loadFromFile("assets/text_cursor.png");
    cursorSprite.setTexture(cursorTexture);
    cursorSprite.setOrigin(0, cursorTexture.getSize().y/2);
    cursorSprite.setScale(0.6, 0.6);

    cursorShowing = false;

    chatOverlayTexture.loadFromFile("assets/chatOverlay.png");
    chatOverlayTexture.setSmooth(true);
    chatOverlayTexture.setRepeated(true);
    chatOverlaySprite.setTexture(chatOverlayTexture);

    arenaView = sf::View(sf::Vector2f(0, 0), sf::Vector2f(aspect_ratio.x, aspect_ratio.y));

    islandTexture.loadFromFile("assets/Dirt.jpg");
    lavaTexture.loadFromFile("assets/lava.jpg");

    lavaTexture.setRepeated(true);
    lavaSprite.setTexture(lavaTexture);
    lavaSprite.setPosition(0, 0);
    lavaSprite.setTextureRect(sf::IntRect(0, 0, 20480, 20480));
    lavaSprite.setOrigin(sf::Vector2f(lavaSprite.getTextureRect().width / 2, lavaSprite.getTextureRect().height / 2));

    islandTexture.setRepeated(true);
    islandTexture.setSmooth(true);
    islandShape.setTexture(&islandTexture);
    islandShape.setRadius(1500);
    islandShape.setOrigin(1500,1500);
    islandShape.setPosition(0, 0);
    islandShape.setTextureRect(sf::IntRect(-1500, -1500, 1500*2, 1500*2));
}

void MatchScene::reset()
{
    arenaLoaded = false;
    fireballs.clear();
    iceballs.clear();
    ingameChatStrings.clear();
    for (int i = 0; i < MAX_CHAT_LINES; ++i)
        ingameChat[i].setString("");
    enteredText.clear();
    playerChatText.setString("");
    activeChat = false;
}

GameState MatchScene::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    if(!arenaLoaded)
    {
        playerCount = GameManager::numberOfPlayers;
        players = std::vector<Player>(playerCount);

        startGame();

        updateView(arenaView);
        window.setView(arenaView);
        arenaLoaded = true;
        sendClock.restart();
        fireball_cooldown = 1;
        iceball_cooldown = 1;
        sequenceNumber = 0;
        lastMessageTime = sf::seconds(5);
    }

    uint8_t code;

    while (socket.Read(&code, sizeof(code)))
    {
        switch(code)
        {
            case CommandCodes::ERROR:
            {
                Error errorCode;
                socket.ReadSync(&errorCode, sizeof(errorCode));

                std::cout << GetErrorString(errorCode) << std::endl;

                break;
            }
            case CommandCodes::STATE_UPDATE:
            {
                //std::cout <<"state update from server" << std::endl;
                updateState(elapsed);
                updateView(arenaView);
                window.setView(arenaView);

                break;
            }
            case CommandCodes::GAME_FINISHED:
            {
                for(int i = 0; i < playerCount; i++)
                {
                    uint8_t rank;
                    socket.ReadSync(&rank, sizeof(rank));

                    GameManager::playerRankings[i] = rank;
                }
                auto view = sf::View(sf::FloatRect(0,0,original_ratio.x, original_ratio.y));
                //view.setSize(original_ratio.x, original_ratio.y);
                window.setView(view);

                //send ACK

                //`[16][Player ID (int)][Game ID (int)]`
                uint8_t code = CommandCodes::GAME_FINISHED;
                socket.WriteSync(&code, sizeof(code));

                int32_t playerID = GameManager::getPlayerID();
                socket.WriteSync(&playerID, sizeof(playerID));

                int32_t gameID = GameManager::getGameID();
                socket.WriteSync(&gameID, sizeof(gameID));

                return GameState::POSTGAME_STATE;
            }
            case CommandCodes::PROJ_NOTIFICATION:
            {
                receiveProjectileNotification();
                break;
            }
            case CommandCodes::COLLISION_NOTIFICATION:
            {
                handleCollision();
                break;
            }
            case CommandCodes::CHAT:
            {
                int32_t pid;
                socket.ReadSync(&pid, sizeof(pid));
                std::string text = socket.ReadString();

                for (int i = 0; i < GameManager::numberOfPlayers; ++i)
                    if (GameManager::playerIDs[i] == pid)
                    {
                        text = GameManager::playerNames[i] + ":  " + text;
                        break;
                    }

                ingameChatStrings.push_front(text);
                lastMessageTime = sf::Time::Zero;
                break;
            }
            default:
            {
                std::cout << "Unknown command code " << std::to_string(static_cast<int>(code)) << std::endl;
                break;
            }
        }
    }

    auto fireball_iterator = std::begin(fireballs);

    while (fireball_iterator != std::end(fireballs))
    {
        if (!fireball_iterator->Update(elapsed))
        {
            fireball_iterator = fireballs.erase(fireball_iterator);
        }
        else
        {
            ++fireball_iterator;
        }
    }

    auto iceball_iterator = std::begin(iceballs);
    while (iceball_iterator != std::end(iceballs))
    {
        if (!iceball_iterator->Update(elapsed))
        {
            iceball_iterator = iceballs.erase(iceball_iterator);
        }
        else
        {
            ++iceball_iterator;
        }
    }

    if (fireball_cooldown > 0)
    {
        fireball_cooldown -= elapsed.asSeconds();
        if (fireball_cooldown < 0)
        {
            fireball_cooldown = 0;
        }
    }

    if (iceball_cooldown > 0)
    {
        iceball_cooldown -= elapsed.asSeconds();
        if (iceball_cooldown < 0)
        {
            iceball_cooldown = 0;
        }
    }

    if (GameManager::isFocused)
    {

        if (fireball_cooldown == 0 && players[playerIndex].getStatus() == PlayerStatus::Alive
        && sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            createFireball(window);
        }

        if (iceball_cooldown == 0 && players[playerIndex].getStatus() == PlayerStatus::Alive
        && sf::Mouse::isButtonPressed(sf::Mouse::Right))
        {
            createIceball(window);
        }

        for(unsigned int i = 0; i < events.size(); i++)
        {
            if(events[i].type == sf::Event::TextEntered && events[i].text.unicode == '\t')
            {
                activeChat = !activeChat;
                if (activeChat)
                {
                    playerChatText.setString("Chat: " + enteredText);
                    cursorShowing = true;
                    cursorCounter = sf::Time::Zero;
                }
                else
                {
                    playerChatText.setString("");
                    enteredText.clear();
                    cursorShowing = false;
                }
            }
        }
    }

    //player update
    // Also sends update to server
    if (!activeChat)
        calculateMovement();
    else
    {
        cursorCounter += elapsed;
        if(cursorCounter.asSeconds() > 1)
        {
            cursorShowing = !cursorShowing;
            cursorCounter = sf::Time::Zero;
        }

        processChat(events);
    }

    lastMessageTime += elapsed;

	// Ryan: I'm commenting these lines out during my merge but we may end up wanting them back
    //sf::Vector2f newPosition = players[playerIndex].getPosition() + (velocity*lastElapsedTime);
    //players[playerIndex].setPosition(newPosition);

    return GameState::INGAME_STATE;
}


void MatchScene::draw(sf::RenderWindow& window)
{
    if(arenaLoaded)
    {
        window.draw(lavaSprite);
        window.draw(islandShape);

        while (ingameChatStrings.size() > MAX_CHAT_LINES)
            ingameChatStrings.pop_back();

        for (auto& fireball : fireballs)
        {
            fireball.Draw(window);
        }

        for(int i = 0; i < playerCount; i++)
        {
            if(players[i].getStatus() == PlayerStatus::Alive)
                players[i].draw(window);
        }

        for (auto& iceball : iceballs)
        {
            iceball.Draw(window);
        }

        if (activeChat || lastMessageTime.asSeconds() <= 5.0f)
        {
            auto view = window.getView();
            // I tried moving this elsewhere.... But I need the window, so here it is.
            sf::View chatView;
            chatView.setSize(window.getSize().x, window.getSize().y);
            chatView.setCenter(window.getSize().x / 2, window.getSize().y / 2);
            chatView.setViewport(sf::FloatRect(0, 0, 1, 1));
            window.setView(chatView);

            if(activeChat)
                chatOverlaySprite.setTextureRect(sf::IntRect(0,0,450,150));
            else
                chatOverlaySprite.setTextureRect(sf::IntRect(0,0,450,120));

            window.draw(chatOverlaySprite);

            window.draw(playerChatText);
            for (size_t i = 0; i < ingameChatStrings.size(); ++i)
            {
                ingameChat[i].setString(ingameChatStrings[i]);
                window.draw(ingameChat[i]);
            }
            if(cursorShowing)
                window.draw(cursorSprite);

            window.setView(view);
        }
    }

}

void MatchScene::resize(sf::Vector2i ratio)
{
    //resize layout of elements
    original_ratio = ratio;
    aspect_ratio = ratio;

    float textX = playerChatText.getLocalBounds().width + playerChatText.getPosition().x;
    cursorSprite.setPosition(textX + 2, playerChatText.getPosition().y + 10);
}

void MatchScene::exit()
{
    //send logout message
}

void MatchScene::startGame()
{
    //[12][Player ID (int)][Game ID (int)]

    uint8_t code = START_GAME;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    if(!socket.ReadSync(&code, sizeof(code)))
    {
        //TODO: Show that there was a server error
        return;
    }

    //[12][Position (float,float)][...]
    if(code == START_GAME)
    {
        for(int i = 0; i < playerCount; i++)
        {

            if(GameManager::playerIDs[i] == GameManager::getPlayerID())
            {
                playerIndex = i;
                players[i].setAsPlayer();
            }

            sf::Vector2f position;
            socket.ReadSync(&position, sizeof(position));
            players[i].setPosition(position);
            players[i].setPlayerName(GameManager::playerNames[i]);
            players[i].setPlayerColor(GameManager::playerColors[i]);
        }
    }

}

void MatchScene::updateState(sf::Time elapsed)
{
    /**
     * [13][Island Radius (float)][Player State][..]
     *
     * Where `[Player State]` is `[Position (float,float)][Orientation (float)]
     * [Velocity (float, float)][Health (float)][Status (byte)]`
     * and `[Status]` could be dropped, dead, alive, etc.
     */

    float newIslandRadius;
    socket.ReadSync(&newIslandRadius, sizeof(newIslandRadius));

    int realRadius = (int) newIslandRadius;
    islandShape.setRadius(realRadius);
    islandShape.setOrigin(realRadius,realRadius);
    islandShape.setTextureRect(sf::IntRect(-realRadius, -realRadius, realRadius*2, realRadius*2));

    for(int i = 0; i < playerCount; i++)
    {
        sf::Vector2f position;
        socket.ReadSync(&position, sizeof(position));

        float orientation;
        socket.ReadSync(&orientation, sizeof(orientation));

        sf::Vector2f velocity;
        socket.ReadSync(&velocity, sizeof(velocity));

        float health;
        socket.ReadSync(&health, sizeof(health));

        PlayerStatus status;
        socket.ReadSync(&status, sizeof(status));

        players[i].update(position, orientation, velocity, health, status);
    }
}

void MatchScene::updateView(sf::View& view)
{
    sf::Vector2f top = {0, 0};
    sf::Vector2f bot = {0, 0};
    sf::Vector2f left = {0, 0};
    sf::Vector2f right = {0, 0};

    sf::Vector2f aggregate_center = {0, 0};
    sf::Vector2u average_size = {0, 0};

    // Get most extreme positions for each of the four directions
    for (Player player : players)
    {

        if(player.getStatus() != PlayerStatus::Alive)
            continue;

        sf::Vector2f position = player.getPosition();
        average_size += sf::Vector2u(64, 64);

        if (position.y < top.y)
        {
            top = position;
        }

        if (position.y > bot.y)
        {
            bot = position;
        }

        if (position.x < left.x)
        {
            left = position;
        }

        if (position.x > right.x)
        {
            right = position;
        }

        aggregate_center.x += position.x;
        aggregate_center.y += position.y;
    }

    average_size.x /= players.size();
    average_size.y /= players.size();

    aggregate_center.x = aggregate_center.x / players.size() - average_size.x / 2;
    aggregate_center.y = aggregate_center.y / players.size() - average_size.y / 2;

    // Get the minimum bounds needed to see all players
    sf::Vector2f bounds = {right.x - left.x + average_size.x * 10, bot.y - top.y + average_size.y * 10};

    sf::Vector2f center = {0, 0};


    if(bounds.x < 0.7f*aspect_ratio.x)
        bounds.x = 0.7f*aspect_ratio.x;

    // Convert bounds to the appropriate aspect ratio
    double ratio = (double)aspect_ratio.x / (double)aspect_ratio.y;
    if (bounds.x / bounds.y > ratio)
    {
        // height is too small
        bounds.y = bounds.x / ratio;
    }
    else if (bounds.x / bounds.y < ratio)
    {
        // width is too small
        bounds.x = bounds.y * ratio;
    }

    // Center point should be the average y of the top and bot
    // and the average x of the left and right
    center.x = (right.x + left.x) / 2;
    center.y = (top.y + bot.y) / 2;

    view.setCenter(center);
    view.setSize(bounds);
}

void MatchScene::calculateMovement()
{
    bool w = false;
    bool a = false;
    bool s = false;
    bool d = false;

    if (GameManager::isFocused)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            w = true;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            a = true;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            s = true;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            d = true;
        }
    }

    float elapsed = sendClock.getElapsedTime().asSeconds();
    if (elapsed > (1.0 / MAX_SEND_RATE))
    {
        sendClock.restart();
        sendUpdateToServer(w, a, s, d);
    }
}

void MatchScene::sendUpdateToServer(bool w, bool a, bool s, bool d)
{
    int message_size = 1 + 4 + 4 + 4 + 4;
    uint8_t message[message_size];

    int32_t gameID = GameManager::getGameID();
    int32_t playerID = GameManager::getPlayerID();

    message[0] = static_cast<uint8_t>(CommandCodes::STATE_UPDATE);
    std::memcpy(&message[1], &gameID, 4);
    std::memcpy(&message[5], &playerID, 4);
    std::memcpy(&message[9], &sequenceNumber, 4);

    std::memcpy(&message[13], &w, 1);
    std::memcpy(&message[14], &a, 1);
    std::memcpy(&message[15], &s, 1);
    std::memcpy(&message[16], &d, 1);

    socket.WriteSync(message, message_size);
}


void MatchScene::createFireball(sf::RenderWindow& window)
{
    sf::Vector2f mouse_position = window.mapPixelToCoords(sf::Mouse::getPosition(window));
    sf::Vector2f sprite_position = players[playerIndex].getPosition();
    sf::Vector2f relative_position = mouse_position - sprite_position;

    float angle = atan(relative_position.y / relative_position.x) * 180 / M_PI + 90;
    if (relative_position.x < 0)
    {
        angle += 180;
    }
    angle = fmod(angle, 360);

    float rotation_radians  = angle - 90;
    rotation_radians = (rotation_radians < 0) ? rotation_radians + 360 : rotation_radians;
    rotation_radians = rotation_radians * (M_PI / 180);

    sf::Vector2f velocity = {static_cast<float>(FIREBALL_VELOCITY * cos(rotation_radians)),
                static_cast<float>(FIREBALL_VELOCITY * sin(rotation_radians))};


    //`[14][Player ID (int)][Game ID (int)][Projectile Type (byte)][Orientation (float)][Velocity (float, float)]`
    uint8_t code = CommandCodes::PROJ_NOTIFICATION;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    ProjectileTypes projType = ProjectileTypes::Fireball;
    socket.WriteSync(&projType, sizeof(projType));

    socket.WriteSync(&angle, sizeof(angle));

    socket.WriteSync(&velocity, sizeof(velocity));

    fireball_cooldown = FIREBALL_LIFESPAN;
}

void MatchScene::createIceball(sf::RenderWindow& window)
{
    sf::Vector2f mouse_position = window.mapPixelToCoords(sf::Mouse::getPosition(window));
    sf::Vector2f sprite_position = players[playerIndex].getPosition();
    sf::Vector2f relative_position = mouse_position - sprite_position;

    float angle = atan(relative_position.y / relative_position.x) * 180 / M_PI + 90;
    if (relative_position.x < 0)
    {
        angle += 180;
    }
    angle = fmod(angle, 360);

    float rotation_radians  = angle - 90;
    rotation_radians = (rotation_radians < 0) ? rotation_radians + 360 : rotation_radians;
    rotation_radians = rotation_radians * (M_PI / 180);

    sf::Vector2f velocity = {static_cast<float>(ICEBALL_VELOCITY * cos(rotation_radians)),
                static_cast<float>(ICEBALL_VELOCITY * sin(rotation_radians))};


    //`[14][Player ID (int)][Game ID (int)][Projectile Type (byte)][Orientation (float)][Velocity (float, float)]`
    uint8_t code = CommandCodes::PROJ_NOTIFICATION;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    ProjectileTypes projType = ProjectileTypes::Iceball;
    socket.WriteSync(&projType, sizeof(projType));

    socket.WriteSync(&angle, sizeof(angle));

    socket.WriteSync(&velocity, sizeof(velocity));

    iceball_cooldown = ICEBALL_LIFESPAN;
}

void MatchScene::receiveProjectileNotification()
{
    //`[14][Projectile ID (byte)][Projectile Type (byte)][Position (float, float)][Orientation(float)][Velocity(float, float)]`
    int8_t projectileID;
    socket.ReadSync(&projectileID, sizeof(projectileID));

    ProjectileTypes projType;
    socket.ReadSync(&projType, sizeof(projType));

    sf::Vector2f position;
    socket.ReadSync(&position, sizeof(position));

    float orientation;
    socket.ReadSync(&orientation, sizeof(orientation));

    sf::Vector2f velocity;
    socket.ReadSync(&velocity, sizeof(velocity));

    switch (projType)
    {
        case ProjectileTypes::Fireball:
        {
            //sf::Vector2f position, sf::Vector2f velocity, float rotation, uint8_t ID
            Fireball fireball(position, velocity, orientation, projectileID);
            fireballs.push_back(fireball);
            break;
        }
        case ProjectileTypes::Iceball:
        {
            Iceball iceball(position, velocity, orientation, projectileID);
            iceballs.push_back(iceball);
            break;
        }
    }
}

void MatchScene::handleCollision()
{
    //`[15][Sequence Number (int)][projectile ID(byte)][Player ID (int)]`
    socket.ReadSync(&sequenceNumber, sizeof(sequenceNumber));

    uint8_t projID;
    socket.ReadSync(&projID, sizeof(projID));

    int32_t playerID;
    socket.ReadSync(&playerID, sizeof(playerID));

    auto fireball_iterator = std::begin(fireballs);
    while (fireball_iterator != std::end(fireballs))
    {
        if (fireball_iterator->getID() == projID)
        {
            fireball_iterator = fireballs.erase(fireball_iterator);
            break;
        }
        else
        {
            ++fireball_iterator;
        }
    }

    auto iceball_iterator = std::begin(iceballs);
    while (iceball_iterator != std::end(iceballs))
    {
        if (iceball_iterator->getID() == projID)
        {
            iceball_iterator = iceballs.erase(iceball_iterator);

            for (int i = 0; i < playerCount; ++i)
            {
                if (GameManager::playerIDs[i] == playerID)
                {
                    players[i].SetFrozen();
                    break;
                }
            }
            break;
        }
        else
        {
            ++iceball_iterator;
        }
    }
}

void MatchScene::processChat(std::vector<sf::Event>& events)
{
    for(unsigned int i = 0; i < events.size(); i++)
    {
        if(events[i].type == sf::Event::TextEntered)
        {
            // Strip any leading whitespace
            size_t firstChar = 0;
            while (firstChar < enteredText.size())
            {
                if (enteredText[firstChar] != ' ' && enteredText[firstChar] != '\t')
                    break;
                else
                    ++firstChar;
            }
            enteredText = enteredText.substr(firstChar);

            //enter key
            if(events[i].text.unicode == 13)
            {
                // Don't send empty messages
                if (enteredText.size())
                {
                    sendChat();
                    enteredText.clear();
                    activeChat = false;
                    playerChatText.setString("");
                    cursorShowing = false;
                    lastMessageTime = sf::Time::Zero;
                    return;
                }
            }
            //backspace
            else if(events[i].text.unicode == 8)
            {
                if (enteredText.size() > 0)
                    enteredText.erase(enteredText.size()-1);
            }
            else
            {
                // Allow reasonable characters only
                sf::Uint32 val = events[i].text.unicode;
                if (val > 31 && val < 128)
                {
                    if (val != '\t')
                    {
                        sf::Text fake;
                        fake.setFont(m_font);
                        fake.setCharacterSize(18);
                        fake.setString(GameManager::getPlayerName() + enteredText);
                        if(fake.getGlobalBounds().width <= MAX_CHAT_WIDTH)
                        {
                            enteredText += val;
                        }
                    }
                }
            }

            playerChatText.setString("Chat: " + enteredText);
            float textX = playerChatText.getLocalBounds().width + playerChatText.getPosition().x;
            cursorSprite.setPosition(textX + 2, playerChatText.getPosition().y + 10);
            cursorShowing = true;
        }
    }
}

void  MatchScene::sendChat()
{
    TCPSocket& socket = GameManager::getTCPSocket();

    uint8_t code = CHAT;
    socket.WriteSync(&code, sizeof(code));

    int32_t playerID = GameManager::getPlayerID();
    socket.WriteSync(&playerID, sizeof(playerID));

    int32_t gameID = GameManager::getGameID();
    socket.WriteSync(&gameID, sizeof(gameID));

    socket.WriteString(enteredText);
}
