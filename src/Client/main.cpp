#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include "game_manager.h"

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
    sf::Vector2i aspect_ratio = {1280, 700};
    const char* ip = "127.0.0.1";

    if (argc > 1)
    {
        try
        {
            if (argc == 2)
            {
                ip = argv[1];
            }
            else if (argc != 3)
            {
                aspect_ratio.x = std::stoi(argv[1]);
                aspect_ratio.y = std::stoi(argv[2]);
            }
            else
                throw std::exception();
        }
        catch (std::exception&)
        {
            std::cerr << "Bad arguments." << std::endl;
            return -1;
        }
    }

    //sf::Image windowIcon;
    //windowIcon.loadFromFile("assets/icon.png");

    sf::RenderWindow window(sf::VideoMode(aspect_ratio.x, aspect_ratio.y), "Sphere Wizard");
    sf::View view(sf::Vector2f(0, 0), sf::Vector2f(aspect_ratio.x, aspect_ratio.y));

    //window.setIcon(windowIcon.getSize().x, windowIcon.getSize().y, windowIcon.getPixelsPtr());

    GameManager manager(aspect_ratio, ip);


    while (window.isOpen())
    {
        manager.Update(window);

        manager.Draw(window);
    }
    return 0;
}
