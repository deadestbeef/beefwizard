#include "post_game.h"
#include "game_manager.h"

#include <iostream>

PostGame::PostGame(sf::Vector2i ratio):aspect_ratio{ratio},rankingsLoaded{false}
{
    font.loadFromFile("assets/Vera.ttf");

    rankingsText[0].setFont(font);
    rankingsText[0].setString("1st");

    rankingsText[1].setFont(font);
    rankingsText[1].setString("2nd");

    rankingsText[2].setFont(font);
    rankingsText[2].setString("3rd");

    rankingsText[3].setFont(font);
    rankingsText[3].setString("4th");

    rankingsText[4].setFont(font);
    rankingsText[4].setString("5th");

    rankingsText[5].setFont(font);
    rankingsText[5].setString("6th");

    backToMenuText.setFont(font);
    backToMenuText.setString("Back To Menu");
    backToMenuText.setOrigin(backToMenuText.getLocalBounds().width/2, backToMenuText.getLocalBounds().height/2);

    for(int i = 0; i < MAX_PLAYERS; i++)
    {
        rankingsText[i].setCharacterSize(45);
        rankingsText[i].setOrigin(rankingsText[i].getLocalBounds().width/2, rankingsText[i].getLocalBounds().height/2);
        playerNames[i].setFont(font);
    }

    resize(ratio);
}

void PostGame::reset()
{
    //nothing yet
    rankingsLoaded = false;
}

GameState PostGame::update(sf::RenderWindow& window, std::vector<sf::Event>& events, sf::Time elapsed)
{
    if(!rankingsLoaded)
    {
        auto size = window.getView().getSize();
        aspect_ratio = sf::Vector2i((int)size.x, (int)size.y);
        loadRankings();
    }

    bool windowFocused = GameManager::isFocused;

    sf::Vector2f mousePos = sf::Vector2f(sf::Mouse::getPosition(window).x,
                                         sf::Mouse::getPosition(window).y);

    if(windowFocused && backToMenuText.getGlobalBounds().contains(mousePos))
    {
        backToMenuText.setScale(1.5f,1.5f);
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            return GameState::MENU_STATE;
        }
    }
    else
        backToMenuText.setScale(1,1);

    return GameState::POSTGAME_STATE;
}

void PostGame::draw(sf::RenderWindow& window)
{
    if(rankingsLoaded)
    {
        for(int i = 0; i < playerCount; i++)
        {
            players[i].draw(window);
            window.draw(playerNames[i]);
            window.draw(rankingsText[i]);
        }
    }

    window.draw(backToMenuText);
}

void PostGame::resize(sf::Vector2i ratio)
{
    backToMenuText.setPosition(ratio.x/2, ratio.y-50);
    if(rankingsLoaded)
    {
        switch (playerCount)
        {
            case 1:
            {
                players[0].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2));
                playerNames[0].setPosition(ratio.x/2, ratio.y/2+50);
                rankingsText[0].setPosition(ratio.x/2, ratio.y/2+100);
                break;
            }
            case 2:
            {
                players[0].setPosition(sf::Vector2f((ratio.x/2) - 100, ratio.y/2));
                playerNames[0].setPosition((ratio.x/2) - 100, ratio.y/2+50);
                rankingsText[0].setPosition((ratio.x/2) - 100, ratio.y/2+100);

                players[1].setPosition(sf::Vector2f((ratio.x/2) + 100, ratio.y/2));
                playerNames[1].setPosition((ratio.x/2) + 100, ratio.y/2+50);
                rankingsText[1].setPosition((ratio.x/2) + 100, ratio.y/2+100);
                break;
            }
            case 3:
            {
                players[0].setPosition(sf::Vector2f((ratio.x/2) - 200, ratio.y/2));
                playerNames[0].setPosition((ratio.x/2) - 200, ratio.y/2+50);
                rankingsText[0].setPosition((ratio.x/2) - 200, ratio.y/2+100);

                players[1].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2));
                playerNames[1].setPosition(ratio.x/2, ratio.y/2+50);
                rankingsText[1].setPosition(ratio.x/2, ratio.y/2+100);

                players[2].setPosition(sf::Vector2f((ratio.x/2) + 200, ratio.y/2));
                playerNames[2].setPosition((ratio.x/2) + 200, ratio.y/2+50);
                rankingsText[2].setPosition((ratio.x/2) + 200, ratio.y/2+100);
                break;
            }
            case 4:
            {
                players[0].setPosition(sf::Vector2f((ratio.x/2) - 200, ratio.y/2 - 130));
                playerNames[0].setPosition((ratio.x/2) - 200, ratio.y/2+50 - 130);
                rankingsText[0].setPosition((ratio.x/2) - 200, ratio.y/2+100 - 130);

                players[1].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2 - 130));
                playerNames[1].setPosition(ratio.x/2, ratio.y/2+50 - 130);
                rankingsText[1].setPosition(ratio.x/2, ratio.y/2+100 - 130);

                players[2].setPosition(sf::Vector2f((ratio.x/2) + 200, ratio.y/2 - 130));
                playerNames[2].setPosition((ratio.x/2) + 200, ratio.y/2+50 - 130);
                rankingsText[2].setPosition((ratio.x/2) + 200, ratio.y/2+100 - 130);

                players[3].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2 + 130));
                playerNames[3].setPosition(ratio.x/2, ratio.y/2+50 + 130);
                rankingsText[3].setPosition(ratio.x/2, ratio.y/2+100 + 130);
                break;
            }
            case 5:
            {
                players[0].setPosition(sf::Vector2f((ratio.x/2) - 200, ratio.y/2 - 130));
                playerNames[0].setPosition((ratio.x/2) - 200, ratio.y/2+50 - 130);
                rankingsText[0].setPosition((ratio.x/2) - 200, ratio.y/2+100 - 130);

                players[1].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2 - 130));
                playerNames[1].setPosition(ratio.x/2, ratio.y/2+50 - 130);
                rankingsText[1].setPosition(ratio.x/2, ratio.y/2+100 - 130);

                players[2].setPosition(sf::Vector2f((ratio.x/2) + 200, ratio.y/2 - 130));
                playerNames[2].setPosition((ratio.x/2) + 200, ratio.y/2+50 - 130);
                rankingsText[2].setPosition((ratio.x/2) + 200, ratio.y/2+100 - 130);

                players[3].setPosition(sf::Vector2f((ratio.x/2) - 100, ratio.y/2 + 130));
                playerNames[3].setPosition((ratio.x/2) - 100, ratio.y/2+50  + 130);
                rankingsText[3].setPosition((ratio.x/2) - 100, ratio.y/2+100  + 130);

                players[4].setPosition(sf::Vector2f((ratio.x/2) + 100, ratio.y/2 + 130));
                playerNames[4].setPosition((ratio.x/2) + 100, ratio.y/2+50 + 130);
                rankingsText[4].setPosition((ratio.x/2) + 100, ratio.y/2+100 + 130);
                break;
            }
            case 6:
            {
                players[0].setPosition(sf::Vector2f((ratio.x/2) - 200, ratio.y/2 - 130));
                playerNames[0].setPosition((ratio.x/2) - 200, ratio.y/2+50 - 130);
                rankingsText[0].setPosition((ratio.x/2) - 200, ratio.y/2+100 - 130);

                players[1].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2 - 130));
                playerNames[1].setPosition(ratio.x/2, ratio.y/2+50 - 130);
                rankingsText[1].setPosition(ratio.x/2, ratio.y/2+100 - 130);

                players[2].setPosition(sf::Vector2f((ratio.x/2) + 200, ratio.y/2 - 130));
                playerNames[2].setPosition((ratio.x/2) + 200, ratio.y/2+50 - 130);
                rankingsText[2].setPosition((ratio.x/2) + 200, ratio.y/2+100 - 130);

                players[3].setPosition(sf::Vector2f((ratio.x/2) - 200, ratio.y/2 + 130));
                playerNames[3].setPosition((ratio.x/2) - 200, ratio.y/2+50 + 130);
                rankingsText[3].setPosition((ratio.x/2) - 200, ratio.y/2+100 + 130);

                players[4].setPosition(sf::Vector2f(ratio.x/2, ratio.y/2 + 130));
                playerNames[4].setPosition(ratio.x/2, ratio.y/2+50 + 130);
                rankingsText[4].setPosition(ratio.x/2, ratio.y/2+100 + 130);

                players[5].setPosition(sf::Vector2f((ratio.x/2) + 200, ratio.y/2 + 130));
                playerNames[5].setPosition((ratio.x/2) + 200, ratio.y/2+50 + 130);
                rankingsText[5].setPosition((ratio.x/2) + 200, ratio.y/2+100 + 130);
                break;
            }
        }
    }
}


void PostGame::exit()
{
    //nothing to do here
}

void PostGame::loadRankings()
{

    playerCount = GameManager::numberOfPlayers;
    players = std::vector<Player>(playerCount);

    for(int i = 0; i < playerCount; i++)
    {
        uint8_t place = GameManager::playerRankings[i] - 1;
        playerNames[place].setString(GameManager::playerNames[i]);
        playerNames[place].setOrigin(playerNames[place].getLocalBounds().width/2, playerNames[place].getLocalBounds().height/2);
        players[place].setPlayerColor(GameManager::playerColors[i]);
        players[place].clearHealth();

        if(GameManager::playerNames[i] == GameManager::getPlayerName())
            players[place].setAsPlayer();
    }

    rankingsLoaded = true;
    resize(aspect_ratio);
}