#include <iostream>
#include "game_manager.h"
#include "login.h"
#include "main_menu.h"
#include "lobby.h"
#include "game_search.h"
#include "match_scene.h"
#include "post_game.h"
#include "leaderboard.h"
#include "tutorial_scene.h"

using std::cout;
using std::endl;

//definition of static members
TCPSocket GameManager::socket;
std::string GameManager::playerName;
int32_t GameManager::playerID = -1;
std::string GameManager::gameName;
int32_t GameManager::gameID = -1;
bool GameManager::isHosting = false;
std::string GameManager::playerNames[MAX_PLAYERS];
int32_t GameManager::playerIDs[MAX_PLAYERS];
sf::Color GameManager::playerColors[MAX_PLAYERS];
uint8_t GameManager::numberOfPlayers = 0;
uint8_t GameManager::playerRankings[MAX_PLAYERS];
bool GameManager::isFocused = true;

sf::Font GameManager::globalFont;
sf::Texture GameManager::playerBase;
sf::Texture GameManager::playerShading;
sf::Texture GameManager::playerHat;
sf::Texture GameManager::enemyHat;
sf::Texture GameManager::fireballTexture;
sf::Texture GameManager::iceballTexture;
sf::Texture GameManager::iceblock;


TCPSocket& GameManager::getTCPSocket()
{
    return socket;
}

std::string GameManager::getPlayerName()
{
    return playerName;
}

void GameManager::setPlayerName(std::string name)
{
    playerName = name;
}

int32_t GameManager::getPlayerID()
{
    return playerID;
}

void GameManager::setPlayerID(uint32_t ID)
{
    playerID = ID;
}

std::string GameManager::getGameName()
{
    return gameName;
}

void GameManager::setGameName(std::string name)
{
    gameName = name;
}

int32_t GameManager::getGameID()
{
    return gameID;
}

void GameManager::setGameID(int32_t ID)
{
    gameID = ID;
}

bool GameManager::isHost()
{
    return isHosting;
}

void GameManager::setHosting(bool hosting)
{
    isHosting = hosting;
}

GameManager::GameManager(sf::Vector2i ratio, const char* ip) : currentState{GameState::LOGIN_STATE}
{

    globalFont.loadFromFile("assets/Vera.ttf");
    playerBase.loadFromFile("assets/PlayerTemplate.png");
    playerBase.setSmooth(true);
    playerShading.loadFromFile("assets/PlayerShading.png");
    playerShading.setSmooth(true);
    playerHat.loadFromFile("assets/WizardHat.png");
    playerHat.setSmooth(true);
    enemyHat.loadFromFile("assets/EnemyWizardHat.png");
    enemyHat.setSmooth(true);
    fireballTexture.loadFromFile("assets/fireball.png");
    fireballTexture.setSmooth(true);
    iceballTexture.loadFromFile("assets/iceball.png");
    iceballTexture.setSmooth(true);
    iceblock.loadFromFile("assets/iceblock.png");
    iceblock.setSmooth(true);

    scenes[GameState::LOGIN_STATE] = new Login(ratio);
    scenes[GameState::MENU_STATE] = new MainMenu(ratio);
    scenes[GameState::LOBBY_STATE] = new Lobby(ratio, socket);
    scenes[GameState::GAME_SEARCH_STATE] = new GameSearch(ratio);
    scenes[GameState::INGAME_STATE] = new MatchScene(ratio, socket);
    scenes[GameState::POSTGAME_STATE] = new PostGame(ratio);
    scenes[GameState::LEADERBOARD_STATE] = new Leaderboard(ratio);
    scenes[GameState::TUTORIAL_STATE] = new TutorialScene(ratio);
    currentScene = scenes[currentState];

    while(socket.ConnectToServer(ip))
    {
        //couldn't connect
    }
}
GameManager::~GameManager()
{
    socket.CloseSocket();
}

void GameManager::Update(sf::RenderWindow& window)
{
    polledEvents.clear();
    sf::Time elapsed = clock.restart();
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            //clean up all scenes before exiting
            for(int i = 0; i < GameState::STATE_COUNT; i++)
            {
                scenes[i]->exit();
            }
            window.close();
        }
        else if(event.type == sf::Event::Resized)
        {
            sf::Vector2i ratio = sf::Vector2i(event.size.width, event.size.height);

            sf::View newView = sf::View(sf::FloatRect(0,0,ratio.x, ratio.y));

            window.setView(newView);

            for(int i = 0; i < GameState::STATE_COUNT; i++)
            {
                scenes[i]->resize(ratio);
            }
        }
        else if (event.type == sf::Event::GainedFocus)
        {
            isFocused = true;
        }
        else if (event.type == sf::Event::LostFocus)
        {
            isFocused = false;
        }
        else
            polledEvents.push_back(event);

    }

    GameState nextState = currentScene->update(window, polledEvents, elapsed);

    if(currentState != nextState)
    {
        currentScene->reset();
        currentScene = scenes[nextState];
        currentState = nextState;
    }

}

void GameManager::Draw(sf::RenderWindow& window)
{
    window.clear(sf::Color::Black);
    currentScene->draw(window);
    window.display();
}
