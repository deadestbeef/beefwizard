#BEEF Protocol
The BEEF Protocol is the networking protocol used in BeefWizard. It describes a series of commands that are interpreted by both the game client and the game server.

Each command sent to/from the server and client is a contiguous sequence of data and is described in the following format:

`[code][Name of Data (type)][Name of Data (type)]`.

The code is an 8-bit unsigned value that describes the command, and the data associated with the command follows the code.

For commands that can include a variable amount of data, they must include the number of elements before the series of data. These codes can describe a variable number of a single datatype or a variable number of a combination of datatypes. They are described with the following formats:

`[code][Number of Elements (int)][Name of Data (type)][...]`

`[code][Number of Elements (int)][[Name of Data (type)],[Name of Data (type)]][...]`

Types which are a collections of basic data types are separated by commas. For example, a location might be described as `(float,float)`.

##Data Types
* `byte` - an 8-bit unsigned integer
* `short` - a 16-bit unsigned integer
* `int` - 32-bit signed integer
* `float` - a 32-bit floating point
* `str` - character strings, which begin with a 16-bit unsigned integer describing the length in characters


##Command Errors
Only the server issues errors to clients, and it does so in the format:

`[0][Error Code (short)]`

Apart from the command specific errors listed below, the server will inform the client if it cannot understand a command or if that command does not apply to them.

If the client receives a command that it doesn't understand or does not apply to them, it does not send an error to the server (it ignores it).


##Basic Commands

###Register User
Registering is the process of the server acknowledging a player as being able to make requests to the server.

#####Client ➡ Server:
When a player wants access to the server, they must request that the server register them into the system. A player uses their player name as their identifier. BEEF currently does not support passwords, but could be extended to do so.

* `[1][Player Name (str)]`

#####Server ➡ Client:
Upon receiving a request to register a player, the server will provide them with the Player ID that player is expected to use for the rest of that session. If the player name is already in use, the server will return an error.

* `[1][Player ID (int)]` (success)

* `[0][1]`  (error – player name already taken)

###Create Game
Creating a game is the process of the server creating a game that other players can join. The player who creates a game is considered its host.

#####Client ➡ Server:
If a player wants to create a game, it must make a server request for a game to be created for it.

* `[2][Player ID (int)]`

#####Server ➡ Client:
Upon receiving a create game request, the server will create and return a game ID. This game ID serves as the identifier for the game lobby and the game itself once it has started. If the player is already hosting a game or is already in a game, the server will return an error.

* `[2][Game ID (int)]` (success)
* `[0][2 (short)]`   (error – player already hosting a game)
* `[0][3 (short)]`   (error – player already in a different game)

###List Games
Listing games is the process of the server issuing a list of games that a player can currently join.

#####Client ➡ Server:
If a player wants to see a list of available games, the player must request that list from the server. The player is expected to make this request again to get a more up-to-date list of games.

* `[3][Player ID (int)]`

#####Server ➡ Client:
Upon receiving a request for the game list, the server will return the number of games available to join, and then a Game Name and Game ID for each available game. If there are no games available to join, the number of games will be zero and will not be followed by any data.

* `[3][Number of Games (int)][[Game Name (str)],[ Game ID (int)],[Num Players (byte)]][...]`
* `[3][0 (int)]`  (no games to join)

###Join Game
Joining a game is the process of a player being associated with an existing game.

####Join Request
#####Client ➡ Server:
If a player wants to join an existing game, the player must make a request to the server that includes the Game ID of the game they want to join.

* `[4][Player ID (int)][Game ID (int)]`

#####Server ➡ Client:
Upon receiving a request to join a game, server will acknowledge the player has joined by sending the Game ID back along with the player data of the other players currently in the game. If the requested game cannot be found, the requested game is full, or the player is already in a game, the server will respond with an error.

* `[4][Game ID(int)][Player Count(byte)][[Player Name(str)],[Player ID(int)]][...]`
* `[0][3 (short)]` (error – player already in game)
* `[0][4 (short)]` (error – game not found)
* `[0][5 (short)]` (error – game full)

#### Other Players Joining
#####Server ➡ Client:
When a player joins a game, the server will inform all other players in that game that a new player has joined. It will not send this notification to the player that just joined.

* `[5][Player ID (int)][Player Name (str)]`

###Exit Game
Exiting a game is the process of the server removing a player's association with that game. If a host exits a game, it is considered being in an invalid state and all other players are removed from that game.

####Other Players Exiting
#####Server ➡ Client:
When a player exits a game, the server will inform all other players in that game that a player has left.

* `[6][Player ID (int)][Player Name (str)]`

####Host Exiting
#####Server ➡ Client:
If the host of a game (the player that created it) exits a game, the game is in an errored state and all players in that game will be notified.

* `[0][6 (short)]` (error – host left)

####Exiting Request
#####Client -> Server:
When a player wants to exit a game, they must make a request to the server that includes the Game ID of the game they want to leave.

* `[7][Player ID (int)][ Game ID (int)]`

#####Server ➡ Client:
Upon receiving a request to exit a game, the server will respond with an acknowledgement to the player making the request containing the Game ID. Duplicate requests by the client will be a no-op server-side, but will be re-acknowledged. Duplicate acknowledgements client-side are ignored.

* `[7][Game ID (int)]`

###Load Game
Loading the game is the process of the server and clients getting ready for the match to start.

#####Client ➡ Server:
The host of a game has the ability to issue a Load Game request to the server. This will inform the server that the game will begin soon and that players will need information about the game.

* `[8][Player ID (int)][Game ID (int)]`

#####Server ➡ Client:
Upon receiving a request to load the game, the server will send information needed to load the game to all players in the game. Once the server recieves this request, it is no longer in a state to allow additional players to join. The player must remember the number of players and the ordering of players in this message as it will be used to allow the server to omit the Player ID in all in-game state updates.

* `[8][Game Info][Number of Players (byte)][Player Info][...]`

Where `[Game Info]` is currently defined as 0 bytes (it could be map info, obstacle info, other stuff) and `[Player Info]` is `[[Player ID (int)],[Player Color (byte,byte,byte)]]`

###Unregister
Unregistering is the process of a player no longer being able to make requests of the server. The player making this request is removed from the server's list of players.

#####Client ➡ Server:
When a player wants to closes the client, it must make an unregister request to the server. The client will wait for an acknowledgement from the server before closing.

* `[9][Player ID(int)]`

#####Server ➡ Client:
Upon receiving a request to unregister a player, the server will remove that player from its list of players and will then acknowledge the client. Duplicate requests by the client will be a no-op server-side, but will be re-acknowledged.

* `[9][Player ID(int)]`

###Lobby/In-Game Chat
Chatting is the process of sharing messages with other players in your game. The chat commands are the same in a lobby and in-game since they are game specific. Players cannot chat with one another outside of being in a game.

#####Client ➡ server:
When a player wants to send a message to the other players in their game, it request that the server send a message to all other players.

* `[10][Player ID (int)][Game ID (int)][Message (str)]`

#####Server ➡ client:
Upon receiving a request to relay a message, the server will notify all players that there was a message, who sent it, and what the text the text is.

* `[10][Player ID (int)][Message (str)]`

###Leaderboard
Every player has their match statistics saved, and every client can request the rankings from the server.

The server will respond with the number of all players that have statistics saved and will then send the stats for each player in the order of their rank.

The server stores Player Name, # wins, # games, times in 1st, times in 2nd, times in 3rd, and times in 4th for each player that has logged in.

Currently, ranking is calculated by wins*(wins/games), or the number of wins times their win ratio.

#####Client ➡ Server:
If a player that wants to see the leaderboard, it must make a request to the server.

* `[11][Player ID (int)]`

#####Server ➡ Client:
Upon receiving a request to see the board, the server will respond with the list of current rankings.

* `[11][# of Players Listed (int)][Player Stats][...]`

Where `[Player Stats]` is `[Player Name (str)][Number of Wins (int)][Games Played (int)][Average Placement (float)]`


##In-game Protocols
###Game Start
Starting a game is the process of the players and server beginning their state updates and informing one another of their current states.

#####Client ➡ server:
When a player has the game set up and is ready to start, it notifies the server the server.

* `[12][Player ID (int)][Game ID (int)]`

#####Server ➡ Client:
When all players have notified the server that are ready, the server will send a notification to all players that they can start the game and where the players are starting. The order of player positions is the same order as the Load Game notification.

* `[12][Position (float,float)][...]`

###Player State Update
A player state update is the process of players informing the server of their current state, and then the server informing all players of the state they should use for drawing and the base of their next state.

#####Client ➡ Server:
A player is expected to periodically inform the server of their current state. The sequence number will echo the highest sequence number received by the client from the server for projectile collision notifications ([15]).

* ~~`[13][Game ID (int)][Client ID (int)][Sequence Number (int)][Position (float,float)][Orientation (float)][Velocity (float, float)]`~~
* `[13][Game ID (int)][Client ID (int)][Sequence Number (int)][W (byte)][A (byte)][S (byte)][D (byte)]`

#####Server ➡ Client:
After the server recieves all of the player state updates, it perfoms any calculations it needs to do and then sends a message to all players with the current state of players they should use. The order of player information is the same order as the Load Game notification.

* `[13][Island Radius (float)][Player State][..]`

Where `[Player State]` is `[Position (float,float)][Orientation (float)][Velocity (float, float)][Health (float)][Status (byte)]` and `[Status]` could be dropped, dead, alive, etc.

###Projectile Creation
Projectile creation is the process of the server informing players that a projectile has been created and they should keep track of it.

#####Client ➡ Server:
When a player wants to launch a projectile, it notifies the server.

* `[14][Player ID (int)][Game ID (int)][Projectile Type (byte)][Orientation (float)][Velocity (float, float)]`

#####Server ➡ Client:
When the server recieves a projectile creation notification, it informs all players in the game that a projectile has been created.

* `[14][Projectile ID (byte)][Projectile Type (byte)][Position (float, float)][Orientation(float)][Velocity(float, float)]`

###Projectile Collision
Projectile collision is the process of the server detecting that there was a collision between a player and a projectile, and that the players should display this and no longer keep track of the projectile.

#####Server ➡ Client:
If the server detects that a projectile has collided with a player, it will inform all players in the game that the collision has happened. It will also increment the sequence number for projectile collisions.

* `[15][Sequence Number (int)][projectile ID(byte)][Player ID (int)]`

###Game Finalization
Game finalization is the process of the server detecting that only one player is left alive and informing all players of the game that the game has ended.

#####Server -> client:
When a game has ended, the server informs all clients. It also details the outcome of the game for all players.

* `[16][Player Placement (byte)][...]`

#####Client ➡ Server:
When a player recieves a Game Finalization notification, it sends an acknowledgement to the server. This informs the server than the player will send no other messages to the server associated with this game.

* `[16][Player ID (int)][Game ID (int)]`